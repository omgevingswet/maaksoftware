// script to list the mocha tests to a json file

const Mocha = require('mocha')
const fs = require('fs')
const path = require('path')

// return an object that corresponds to the given parent
// `output` will contain the same hierarchical structure as the parent
function getParent(output, parent) {
    let grandparent
    if (parent.parent) {
        grandparent = getParent(output, parent.parent)
    } else {
        grandparent = output
    }
    return grandparent[parent.title] = grandparent[parent.title] || {}
}

// add data about a test to the output
// all simple properties that do not start with '_' are copied
function add(output, test) {
    // make sure test information is from a file
    if (test.file) {
        const parent = getParent(output, test.parent)
        const t = {}
        Object.keys(test)
        .filter(key => { return key[0] !== '_' })
        .map(key => {
            const value = test[key]
            if (typeof value !== 'object' && value !== void 0) {
                t[key] = value
            }
        })
        t.file = path.relative(__dirname, t.file)
        parent[test.title] = t
    }
}

// custom reporterer that output simple JSON objects
function JsonReporter(runner, options) {
    const output = options.reporterOptions.output
    runner.on('pass', add.bind(null, output))
    runner.on('fail', add.bind(null, output))
}

const allTests = {};

const mocha = new Mocha({
    reporter: JsonReporter,
    reporterOptions: { output: allTests }
})

const testDir = 'test'

// Add each .js file to the mocha instance
fs.readdirSync(testDir).filter(function(file){
    // Only keep the .js files
    return file.substr(-3) === '.js';
}).forEach(function(file){
    mocha.addFile(
        path.join(testDir, file)
    );
});

mocha.run(() => {
    console.log(JSON.stringify(allTests,null,'  '))
}).catch(e => { console.log(e) })
