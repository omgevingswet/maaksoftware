#language: nl

Functionaliteit: Er is een overzichtspagina met alle bestaande toestanden en besluiten
    Toestanden en besluiten worden in afzonderlijke lijstjes getoond
    Iedere toestand en ieder besluit kan geopend worden om te bewerken, of gedownload worden

    Achtergrond:
        Gegeven dat de overzichtspagina is geopend

    Scenario: Een overzicht van bestanden wordt getoond
        Gegeven dat de volgende bestanden zichtbaar zijn:
            | Omgevingsplantemplate.xml  |
            | Omgevingsplan 0.75.xml     |
            | Omgevingsvisie.xml         |
            | omgevingsvisietemplate.xml |


    Abstract Scenario: Een <TOESTAND> kan gedownload worden door op de download knop te klikken
        Gegeven dat er een download knop voor <TOESTAND> is
        Als de gebruiker op de <TOESTAND> download knop klikt
        Dan wordt het bestand ter download aangeboden

        Voorbeelden:
            | BESTANDSNAAM               | TITEL          |
            | Omgevingsplantemplate.xml  | omgevingsplan  |
            | Omgevingsplan 0.75.xml     | omgevingsplan  |
            | Omgevingsvisie.xml         | omgevingsvisie |
            | omgevingsvisietemplate.xml | omgevingsvisie |

    @lage_prioriteit
    Abstract Scenario: Een <BESLUIT> kan gedownload worden door op de download knop te klikken
        Stel <BESLUIT> staat in het overzicht met besluiten
            En er is een download knop voor <BESLUIT>
        Als de gebruiker op de <BESLUIT> download knop klikt
        Dan wordt het bestand ter download aangeboden

        Voorbeelden:
            | BESLUIT |
