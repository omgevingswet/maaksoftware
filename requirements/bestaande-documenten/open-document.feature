#language: nl

Functionaliteit: Omgevingsdocument openen
Analisten moeten omgevingsdocumenten kunnen openen om voorbeelddocumenten te maken. Hierbij geldt dat er verschillende versies van een document kunnen bestaan, afhankelijk van de versie van het toepassingsprofiel.

Achtergrond:
    Gegeven dat de overzichtspagina is geopend

Abstract Scenario: Een Analist heeft Maaksoftware op het homescherm staan en klikt op <BESTANDSNAAM>. De gebruiker verwacht dat het document in de Editor geopend wordt.

    Gegeven dat het voorbeelddocument genaamd <BESTANDSNAAM> in het overzicht staat
    Als de analist op <BESTANDSNAAM> klikt
    Dan wordt de editor geopend
        En dan staat <TITEL> op de eerste regel
    En staat de tekstcursor in bewerkbare tekst van het document

    Voorbeelden:
        | BESTANDSNAAM               | TITEL          |
        | Omgevingsplantemplate.xml  | omgevingsplan  |
        | Omgevingsplan 0.75.xml     | omgevingsplan  |
        | Omgevingsvisie.xml         | omgevingsvisie |
        | omgevingsvisietemplate.xml | omgevingsvisie |

 @lage_prioriteit @niet_geimplementeerd
    Abstract Scenario: Een <BESLUIT> kan geopend worden door erop te klikken
        Stel <BESLUIT> staat in het overzicht met besluiten
        Als de gebruiker <BESLUIT> aanklikt
        Dan wordt de editor geopend
            En wordt de titel van <BESLUIT> in de editor getoond

        Voorbeelden:
            | BESLUIT |
            |         |
