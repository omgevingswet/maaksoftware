<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:c="urn:c" xmlns:saxon="http://saxon.sf.net/" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">
  <xsl:param name="path"/>
  <xsl:function name="c:file-line">
    <!--
    <xsl:sequence select="concat($path,':',saxon:line-number(),'.&#10;')"/>
    -->
  </xsl:function>
  <!-- check that @as is used everywhere -->
  <xsl:template name="checkPresenceOfTypes">
    <xsl:for-each select="//xsl:template|//xsl:variable|//xsl:function|//xsl:param">
      <xsl:if test="not(@as)">
        <xsl:message>
          <xsl:sequence select="concat('Missing &quot;as&quot; attribute on element [',local-name(),'].&#10;')"/>
          <xsl:sequence select="c:file-line()"/>
        </xsl:message>
      </xsl:if>
    </xsl:for-each>
  </xsl:template>
  <!-- check that no aspecific element() definitions are used -->
  <xsl:template name="checkElementTypes">
    <xsl:for-each select="//@as[starts-with(.,'element()')]">
      <xsl:message>
        <xsl:sequence select="concat('Missing element type in &quot;as&quot; attribute on element [',../local-name(),'].&#10;')"/>
        <xsl:sequence select="c:file-line()"/>
      </xsl:message>
    </xsl:for-each>
  </xsl:template>
  <!-- check that no aspecific attribute() definitions are used -->
  <xsl:template name="checkAttributeTypes">
    <xsl:for-each select="//@as[starts-with(.,'attribute()')]">
      <xsl:message>
        <xsl:sequence select="concat('Missing attribute type in as attribute on ',local-name(),'.&#10;')"/>
        <xsl:sequence select="c:file-line()"/>
      </xsl:message>
    </xsl:for-each>
  </xsl:template>
  <!-- never use xsl:value of -->
  <xsl:template name="checkValueOf">
    <xsl:for-each select="//xsl:value-of">
      <xsl:message>
        <xsl:sequence select="'Use xsl:sequence instead of xsl:value-of.&#10;'"/>
        <xsl:sequence select="c:file-line()"/>
      </xsl:message>
    </xsl:for-each>
  </xsl:template>
  <xsl:template name="checkxslt2">
    <xsl:call-template name="checkPresenceOfTypes"/>
    <xsl:call-template name="checkElementTypes"/>
    <xsl:call-template name="checkAttributeTypes"/>
    <xsl:call-template name="checkValueOf"/>
  </xsl:template>
  <xsl:template match="/xsl:stylesheet">
    <xsl:if test="@version='2.0'">
      <xsl:call-template name="checkxslt2"/>
    </xsl:if>
  </xsl:template>
</xsl:stylesheet>
