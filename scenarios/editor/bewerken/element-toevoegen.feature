#language: nl

Functionaliteit: Element toevoegen binnen een geselecteerd element.
een gebruiker moet, wanneer dit volgens het schema en toepassingsprofiel mag, een kind element toe kunnen voegen aan een geselecteerd element
Elementen die getoond worden in de drop-down zijn enkel degene die volgens het schema en profiel in de huidige situatie op de huidige plek toegevoegd mogen worden
Wanneer bijvoorbeeld in een hoofdstuk zowel een paragraaf als een artikel mag, maar niet samen, moet het alleen mogelijk zijn uit beide te kiezen wanneer het hoofdstuk nog geen van beide bevat.

Scenario: Voor het element met focus of een ouder-element gekozen worden uit een aanpassend menu bovenin de editor

Scenario: Bij het toevoegen kan de gebruiker kiezen of het toe te voegen element aan het begin of eind van het geselecteerde element wordt toegevoegd

Scenario: Wanneer de selectie met de cursor in een kind-element van het geselecteerde element staat, moet ook gekozen kunnen worden het element voor of na dit element toe te voegen.