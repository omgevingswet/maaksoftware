#language: nl

Functionaliteit: Bewerken van tekst
voor het van de tekst wordt gebruik gemaakt van een wysiwyg-editor. Dit is niet hetzelfde als een 'normale' teksteditor als bijvoorbeeld Libre Office of Word. De gebruiker moet zich er juist heel bewust van zijn dat hij in een bepaald onderdeel van het document werkt.