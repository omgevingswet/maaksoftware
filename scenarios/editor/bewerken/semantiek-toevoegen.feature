#language: nl

@geen_ontwerp
Functionaliteit: Een gebruiker kan semantiek op tekstniveau.
In de implementatie komt het toevoegen van semantiek neer op het toevoegen van triples (subject, predicaat, object).
Voor de implemntentatie is nog geen oplossing bedacht, een mogelijke oplossingsrichting is om voor het toevoegen van semantiek een apart gedeelte van het scherm te gebruiken waarin de semantiek op element en op tekstniveau toegevoegd kan worden.

Functionaliteit: Semantiek toevoegen op elementniveau
In het geval van semantiek op elementniveau is het subject altijd het element zelf.

    Scenario: Gebruiker kan de dialoog voor het toevoegen van semantiek openen
        Gegeven dat een element is geselecteerd
        Als de gebruiker heeft geklikt op het toevoegen van semantiek
        Dan wordt een dialoog geopend voor het maken van de tripple
            En is de subject ingevuld met de naam van het element
            En wordt een bijbehorende lijst me predicaten getoond
            En is het object leeg

    Scenario: Gebruiker kan een samentiek triple toevoegen
        Gegeven dat het dialoog voor het toevoegen van semantiek is geopend
        Als een predicaat is geselecteerd
            En een object is ingevuld
            En op de knop voor opslaan is geklikt
        Dan is de semantiek aan het element toegevoegd

    Scenario: Gebruiker kan een semantiek triple verwijderen
        Gegeven dat een element is geselecteerd
            En de gebruiker heeft geklikt op het verwijderen van semantiek
            En het overzicht met semantische triples horend bij het element worden getoond
        Als de gebruiker de te verwijderen triple selecteerd
            En op de knop klikt om deze te verwijderen
            En op OK klikt
        Dan is de geselecteerde semantische triple niet meer van toepassing op het geselecteerde element
            En is de semantische triple niet verwijderd bij andere elementen waar hij nog van toepassing is

    Scenario: Gebruiker kan een semantiek triple bewerken
        Gegeven een element is geselecteerd
            En de gebruiker heeft geklikt op het bewerken van een triple
            En het overzicht met semantische triples horend bij het element worden getoond
        Als de gebruiker de te bewerken triple selecteerd
            En op de knop klikt om deze te bewerken
        Dan opend een dialoog waarin de triple wordt getoond
            En de subject niet te veranderen is
            En het predicaat een dropdown is van mogelijkheden
            En het predicaat staat op de semantiek zoals deze was opgeslagen
            En een ander predicaat gekozen kan worden
            En het object een tekstveld is
            En het object de waarde heeft van de opgeslagen semantiek
            En het object aan te passen is naar een ander waarde

    Scenario: Gebruiker kan een semantiek triple bewerken en dan opslaan
        Gegeven dat een element is geselecteerd
            En de gebruiker heeft geklikt op het bewerken van een triple
            En de gebruiker de te bewerken triple heeft geselecteerd
            En de gebruiker op de knop heeft geklikt om de triple te bewerken
            En de dialoog wordt getoond
        Als de gebruiker de predicaat aanpast naar een ander
            En op de knop klikt om deze weiziging op te slaan
        Dan is de semantische triple voor dit element aangepast
            En is de semantische triple niet aangepast in andere situaties waar hij van toepassing was