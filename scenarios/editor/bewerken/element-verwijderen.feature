#language: nl

Functionaliteit: Element verwijderen binnen een geselecteerd element.
Omdat sommige elementen alleen te selecteren zijn door hun kinderen, kunnen elementen verwijderd worden in het element-navigatie-menu. Selectie van een element zorgt voor selectie van zichzelf en alle ouders en voorouders. Verwijderen van een ouder of voorouder verwijderd ook alle kinderen