#language: nl

Functionaliteit: Navigeren door de tekst
navigeren door de tekst doet de gebruiker door in de inhoudsopgave te klikken. ALs de gehele tekst van de toestand in 1 editor wordt getoond, dan kan de gebruiker ook navigeren door te scrollen.
Het, niet gekozen, alternatief is dat alleen de inhoud van een specifiek geselecteerd element wordt getoond in de editor en navigatie enkel door klikken op de inhoudsopgave verwezelijkt kan worden.