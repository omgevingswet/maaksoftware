# language: nl

Functionaliteit: De editor kan verschillende typen omgevingsdocumenten maken
Een valide document is gebaseerd op het algemene schema en een toepassingsprofiel

Abstract Scenario: Vanuit het scherm Overzicht van documenten kan een nieuw <TYPE> gestart worden.
    Gegeven dat het scherm Overzicht van documenten wordt getoond
        En dat de knop voor het maken van <TYPE> op de pagina staat
        En <SCHEMA> bekend is
        En <PROFIEL> bekend is
    Als ik klik op nieuw <TYPE>
    Dan opend de editor
        En als er een <TEMPLATE> is maakt de editor hier gebruik van
        En de gebruikersomgeving is gebaseerd op <SCHEMA>
        En de gebruikersomgeving maakt gebruik van het <PROFIEL>

    Voorbeelden:
        | TYPE                | SCHEMA   | PROFIEL                                     | TEMPLATE  |
        | omgevingsplan       | IMOP.xsd | toepassingsprofiel_omgevingsplan.xml        |           |
        | omgevingsverodening | IMOP.xsd | toepassingsprofiel_omgevingsverordening.xml |           |
        | omgevingsvisie      | IMOP.xsd | toepassingsprofiel_omgevingsvisie.xml       |           |


Functionaliteit: Er kan een nieuwe toestand gemaakt worden
In een toestand moet de gebruiker tenminste tekst, werkingsbedieden in gml en aanduidingen kunnen invoeren.

Abstract Scenario: In een nieuw document van <TYPE> kan tekst worden ingevoerd

    Voorbeelden:
        | TYPE                | SCHEMA   | PROFIEL                                     | TEMPLATE  |
        | omgevingsplan       | IMOP.xsd | toepassingsprofiel_omgevingsplan.xml        |           |
        | omgevingsverodening | IMOP.xsd | toepassingsprofiel_omgevingsverordening.xml |           |
        | omgevingsvisie      | IMOP.xsd | toepassingsprofiel_omgevingsvisie.xml       |           |

Abstract Scenario: In een nieuw document van <TYPE> kan een werkinsgbedied worden aangegeven

    Voorbeelden:
        | TYPE                | SCHEMA   | PROFIEL                                     | TEMPLATE  |
        | omgevingsplan       | IMOP.xsd | toepassingsprofiel_omgevingsplan.xml        |           |
        | omgevingsverodening | IMOP.xsd | toepassingsprofiel_omgevingsverordening.xml |           |
        | omgevingsvisie      | IMOP.xsd | toepassingsprofiel_omgevingsvisie.xml       |           |

Abstract Scenario: In een nieuw document van <TYPE> kunnen meerdere werkingsgebieden worden aangegeven

    Voorbeelden:
        | TYPE                | SCHEMA   | PROFIEL                                     | TEMPLATE  |
        | omgevingsplan       | IMOP.xsd | toepassingsprofiel_omgevingsplan.xml        |           |
        | omgevingsverodening | IMOP.xsd | toepassingsprofiel_omgevingsverordening.xml |           |
        | omgevingsvisie      | IMOP.xsd | toepassingsprofiel_omgevingsvisie.xml       |           |

Abstract Scenario: In een nieuw document van <TYPE> kan een aanduiding ingevoerd worden

    Voorbeelden:
        | TYPE                | SCHEMA   | PROFIEL                                     | TEMPLATE  |
        | omgevingsplan       | IMOP.xsd | toepassingsprofiel_omgevingsplan.xml        |           |
        | omgevingsverodening | IMOP.xsd | toepassingsprofiel_omgevingsverordening.xml |           |
        | omgevingsvisie      | IMOP.xsd | toepassingsprofiel_omgevingsvisie.xml       |           |

Abstract Scenario: In een nieuw document van <TYPE> kunnen meerdere aanduidingen ingevoerd worden

    Voorbeelden:
        | TYPE                | SCHEMA   | PROFIEL                                     | TEMPLATE  |
        | omgevingsplan       | IMOP.xsd | toepassingsprofiel_omgevingsplan.xml        |           |
        | omgevingsverodening | IMOP.xsd | toepassingsprofiel_omgevingsverordening.xml |           |
        | omgevingsvisie      | IMOP.xsd | toepassingsprofiel_omgevingsvisie.xml       |           |
