#language: nl

@lage_prioriteit
Functionaliteit: Een simpel scherm (geen daadwerkelijke authenticatie / authorisatie noodzakelijk).
de inlog pagina mag gewoon een mockup zijn, doel is het gevoel van een complete applicatie tijdens presentaties.

    Abstract Scenario: Als de gebruiker niet is ingelogd en het overzicht van documenten wil zien, krijgt deze eerst een dialoog om in te loggen.
        Gegeven dat <GEBRUIKER> nog niet is ingelogd
        Als de gebruiker naar de maaksoftware navigeert
        Dan wordt het overzicht niet getoond
            En krijgt de gebruiker dialoog voor inloggen

        Voorbeelden:
        | GEBRUIKER | WACHTWOORD |
        | JaneDoe   | Pass123    |

    Abstract Scenario: De gebruiker kan inloggen met ieder willekeurig wachtwoord
        Gegeven dat <GEBRUIKER> nog niet is ingelogd
            En de gebruiker naar de maaksoftware navigeert
        Als de gebruiker <GEBRUIKER> invult in het naam veld
            En <WACHTWOOD> invult in het wachtwoord veld
            En op Inloggen klikt
        Dan wordt opgeslagen dat de gebruiker is ingelogd
            En wordt het dialoog voor inloggen gesloten
            En kan de gebruiker het overzicht van maaksoftware zien

        Voorbeelden:
        | GEBRUIKER | WACHTWOORD |
        | JaneDoe   | Pass123    |

    Scenario: Een ingelogde gebruiker hoeft niet nogmaals in te loggen
        Gegeven dat <GEBRUIKER> is ingelogd
        Als de gebruiker naar de maaksoftware navigeert
        Dan kan de gebruiker het overzicht van maaksoftware zien
            En wordt er geen dialoog voor inloggen getoond

        Voorbeelden:
        | GEBRUIKER | WACHTWOORD |
        | JaneDoe   | Pass123    |
