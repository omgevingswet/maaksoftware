# Principes omgevingsplan

## De principes voor functies, activiteiten en werkingsgebieden
- Het omgevingsplangebied is volledig gevuld met werkingsgebieden van functies (het resultaat van functietoedeling aan locaties).
- De werkingsgebieden van functies mogen elkaar geheel of gedeeltelijk overlappen.
- Het omgevingsplan bevat alleen regels die rechtsgevolg hebben. Het enkele toedelen van een
functie aan een locatie is niet voldoende voor het hebben van rechtsgevolg, het toedelen van een functie aan een locatie moet met regels ingevuld worden. Daarom moet onder iedere functie minimaal 1 regel over een activiteit vallen, veelal zal dat in ieder geval een regel over gebruik(sactiviteit(en)) zijn.
- Een regel over de gebruiksactiviteit bevat bepalingen over het gebruik van grond en bouwwerken op die grond (onder de Wro wordt dit gebruik in enge zin genoemd), oftewel planologisch gebruik.
- Het werkingsgebied van een gebruiksactiviteit is gelijk aan het werkingsgebied van de functie waarin de gebruiksactiviteit is toegestaan.
- Toedeling van functies aan locaties kent twee vormen: de enkelvoudige functie waarbij functie en gebruiksactiviteit (nagenoeg) gelijk zijn en de samengestelde functie waarbij de functiebenaming een gebied weergeeft waarin meerdere, vaak ook uiteenlopende en gelijkwaardige, gebruiksactiviteiten zijn toegelaten.
- Een functie en een activiteit kunnen dezelfde benaming hebben (bv wonen: functie Wonen met activiteit wonen); dit kan voorkomen bij de enkelvoudige functie.
- De samengestelde functie kan globaal zijn. Hoe globaal dat mag zijn (m.a.w. wanneer is nog sprake van voldoende rechtszekerheid en kenbaarbheid) zal door de bestuursrechter bepaald worden.
- Naast hun juridische betekenis spelen functie en activiteit een rol in de werking van het omgevingsplan in het DSO: In het DSO faciliteren functies het verbeelden, en faciliteren activiteiten het bevragen.

## Vastleggen begrenzing werkingsgebieden
- Het werkingsgebied van een functie is altijd in coördinaten vastgelegd. De locatie van een gebruiksactiviteit kan gerelateerd zijn aan niet exact bepaalde begrenzingen in het werkingsgebied. In dat geval is een beschrijving in woorden van die locatie toegestaan. Voor de
raadpleging en de bevraging is een beschrijving van de locatie in woorden minder wenselijk. Een voorbeeld van het beschrijven van de conditionele begrenzing van het werkingsgebied van een gebruiksactiviteit in woorden: "In het werkingsgebied van de functie Woongebied is in ieder hoekpand de activiteit 'het exploiteren van een winkel' toegelaten." Deze regel wordt gekoppeld aan het werkingsgebied van de functie Woongebied. Daardoor zie je deze regel ook als je de geldende regels voor een niet-hoekpand opvraagt en is ook bij hoekpanden niet zonder menselijke interpretatie vast te stellen of de regel geldt.
- De geometrieën van een omgevingsplan zijn polygonen of volumes; punten en lijnen zijn niet toegestaan.
- De begrenzing van werkingsgebieden van (onderdelen van) het omgevingsplan is altijd exact.

## Regeltekst
- Begripsbepalingen worden uitsluitend opgenomen in het artikel Begrippen in hoofdstuk 1 en niet (ook) op andere plaatsen in de regels.
- Wanneer een specifieke regel een verbijzondering, uitzondering of afwijking is op een andere regel, hetzij uit hetzelfde omgevingsdocument hetzij uit een ander omgevingsdocument (ook van een ander bevoegd gezag), zorgt het model voor een verwijzing naar de regel waarvan wordt verbijzonderd, uitgezonderd of afgeweken. In de regels moet in zo'n geval gebruik gemaakt worden van de standaardtekst: "In afwijking van artikel x.x lid y (van [naam regeling] geldt ...". Dit geldt onder andere wanneer in het omgevingsplan met maatwerkregels wordt afgeweken van regels uit een AMvB of een omgevingsverordening en wanneer in het omgevingsplan zelf algemene regels staan waarvan voor een bepaalde locatie of een bepaald onderwerp met een specifieke regel wordt afgeweken.

## Annotaties
Annotaties met juridische betekenis moeten machineleesbaar zijn maar in het besluit dat in de LVBB bekend is gemaakt ook voor de mens leesbaar zijn. Dat wordt bewerkstelligd door deze annotaties zoveel mogelijk in de vorm van zogenaamde inlines aan te brengen. Reden hiervoor is de juridische borging van verbeelding en functies binnen het stelsel. Daarnaast geeft deze keuze voor de opsteller een eenduidig en transparant proces (zo min mogelijk van de betekenis "onder water").

## De principes voor tekststructuurelementen
1. Document is het overkoepelende element, de kapstok waar alle regels van het omgevingsplan onder hangen.
2. Hoofdstuk en Artikel komen altijd voor.
3. Als er behoefte is aan groepering van Artikelen in een Hoofdstuk wordt Afdeling gebruikt.
4. In hoofdstukken waarin een onderverdeling in Afdelingen (bijvoorbeeld vanwege de omvang van
het hoofdstuk of de verscheidenheid aan onderwerpen in het hoofdstuk) niet volstaat wordt
Paragraaf gebruikt.
5. Bij behoefte aan nog verder gaande onderverdeling wordt Titel gebruikt; dit tekstonderdeel komt
dan tussen Hoofdstuk en Afdeling.
6. Artikelen kunnen worden onderverdeeld in Leden.
7. Leden kunnen niet worden onderverdeeld in Subleden.
8. Alinea kan voorkomen in Artikel en Lid.
9. Alinea, Tabel, Lijst en Plaatje kunnen voorkomen in Artikel en Lid.
10. Voetnoten zijn niet toegestaan.

Onder Artikel en Lid kunnen opsommingen worden gebruikt. Opsommingen kunnen in meerdere niveaus gebruikt worden, zogenaamde geneste opsommingen. De onderdelen van de opsomming op het eerste niveau worden aangegeven met letters, op het tweede niveau met arabische cijfers en op het derde niveau met romeinse cijfers.

## Volgorde van structuur- en andere tekstelementen
In de navolgende tabel is aangeven welke structuur- en andere tekstelementen moeten dan wel kunnen voorkomen in de regeltekst van het omgevingsplan. Ook is aangegeven hoe vaak zo'n structuurelement moet of kan voorkomen en onder welk structuurelement van een hoger niveau het kan c.q. moet voorkomen.


## Volgorde en voorkomen van structuur- en andere tekstelementen
| **Type structuurelement** | **Aantal** | **Mag voorkomen onder** 	      |
| Document 					| 1		 	 | nvt							  |
| Hoofdstuk 				| 1..n 	 	 | Document 					  |
| Titel 					| 0..n 	 	 | Hoofdstuk 					  |
| Afdeling 					| 0..n 	 	 | Hoofdstuk of Titel 			  |
| Paragraaf					| 0..n 	 	 | Afdeling  					  |
| Artikel 					| 1..n 	 	 | Hoofdstuk, Afdeling, Paragraaf |
| Lid 						| 0..n 	 	 | Artikel 						  |

##  Voorkomen van tekstelementen
| **Type** | **Aantal** | **Mag voorkomen onder** |
| Alinea   | 1..n 		| Artikel, lid 		      |
| Tabel    | 0..n 		| Artikel, lid 		   	  |
| Lijst    | 0..n 		| Artikel, Lid 		   	  |
| Plaatje  | 0..n       | Artikel, lid 		   	  |
