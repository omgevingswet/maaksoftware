#language: nl

Functionaliteit: Er is een overzichtspagina met alle bestaande toestanden en besluiten
    De toestanden en besluiten worden in afzonderlijke lijstjes getoond
    Iedere toestand en ieder besluit kan geopend worden om te bewerken, of gedownload worden

    Abstract Scenario: Een <TOESTAND> kan geopend worden door erop te klikken
        Stel <TOESTAND> staat in het overzicht met toestanden
        Als de gebruiker <TOESTAND> aanklikt
        Dan wordt de editor geopend
            En wordt de titel van <TOESTAND> in de editor getoond

        Voorbeelden:
            | TOESTAND                 |
            | Omgevingsplan 0.75.xml |

    @lage_prioriteit
    Abstract Scenario: Een <BESLUIT> kan geopend worden door erop te klikken
        Stel <BESLUIT> staat in het overzicht met besluiten
        Als de gebruiker <BESLUIT> aanklikt
        Dan wordt de editor geopend
            En wordt de titel van <BESLUIT> in de editor getoond

        Voorbeelden:
            | BESLUIT |
            |           |

    Abstract Scenario: Een <TOESTAND> kan gedownload worden door op de download knop te klikken
        Stel <TOESTAND> staat in het overzicht met toestanden
            En er is een download knop voor <TOESTAND>
        Als de gebruiker op de <TOESTAND> download knop klikt
        Dan wordt het bestand ter download aangeboden

        Voorbeelden:
            | TOESTAND                 |
            | Omgevingsplan 0.75.xml |

    @lage_prioriteit
    Abstract Scenario: Een <BESLUIT> kan gedownload worden door op de download knop te klikken
        Stel <BESLUIT> staat in het overzicht met besluiten
            En er is een download knop voor <BESLUIT>
        Als de gebruiker op de <BESLUIT> download knop klikt
        Dan wordt het bestand ter download aangeboden

        Voorbeelden:
            | BESLUIT                 |
