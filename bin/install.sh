#!/usr/bin/env bash

# Copy the latest version of the XML Schema of STOP
# from the KOOP contentrepository Git repository.
#
# This is a public repository, so to run this step you need
# to request access to Jos van den Oever <jos.oever@koop.overheid.nl>

FILE='poc-toepassingsprofiel/schemas/stop/2017-07-06/xsd/stop_2017-07-06.xsd'
OUTPUT='pr04schemas/stop_2017-07-06.xsd'
git archive --remote=git@gitlab.com:koop/contentrepository.git HEAD "${FILE}"  | tar -xO "${FILE}" > "${OUTPUT}"
