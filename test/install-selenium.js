/**
 * selenium-download does exactly what it's name suggests;
 * downloads (or updates) the version of Selenium (& chromedriver)
 * on your localhost where it will be used by Nightwatch.
 */
const selenium = require('selenium-download');
const path = require('path');

const BIN_PATH = path.resolve(__dirname, './bin/');

selenium.ensure(BIN_PATH, (error) => {

    if (error) throw new Error(error);
    /* eslint-disable no-console */
    console.log('✔ Selenium & Chromedriver downloaded to:', BIN_PATH);
    /* eslint-enable no-console */

});
