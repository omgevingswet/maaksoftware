Voor het beschrijven van de van de maaksoftware requirements wordt gebruik gemaakt van de Gherkin syntax.
Gherkin kan in meer dan 60 talen, waaronder nederlands, met een klein beetje structuur bovenop lopende tekst de functionaliteit en voorbeelden van een project omschrijven. Gherkin is ontworpen om makkelijk leer-en leesbaar te zijn voor niet-programmeurs, maar net genoeg structuur te bieden om er diverse test implementaties aan te koppelen. Documentatie van Cucumber en Gherkin zijn te vinden op [https://cucumber.io/docs/reference](https://cucumber.io/docs/reference).

User vereisten kunnen met Gherkin in het nederlands geschreven worden, hier een overzicht van alle beschikbare commando's:

- Functionaliteit (feature)
- Scenario (scenario)
- Abstract Scenario (scenario outline)
- Gegeven | Stel (given)
- En (and)
- Maar (but)
- Als | Wanneer (when)
- Dan (then)
- Achtergrond (background)
- Voorbeelden (examples)

Om gebruik te maken van deze nederlandse vertaling word ieder testbestand begonnen met

```
# language: nl
```

Een voorbeeld van een .feature bestand in Gherkin syntax is:

```
  Functionaliteit: Een korte beschrijving van de gewenste functionaliteit
    Uitgebreide omschrijving van de functionaliteit die eventueel ingaat op de waarde voor management
    De omvang van de functionaliteit
    Alle extra informatie die de gewenste functionaliteit beter overdraagt

    Scenario: Een 'User Story',
      Gegeven Een bestaande situatie
           En Een andere bestaande situatie
          Als Een gebruiker x doet
           En y doet
           En z doet
          Dan Wordt de volgende testbare uitkomst bereikt
           En Iets anders dat ook testbaar is wordt ook bereikt

    Scenario: Een andere situatieschets
      ...
```

Als je een test met meerdere inputs wil testen, bijvoorbeeld meerdere documenten, gebruikers types e.d. kan gebruik gemaakt worden van Abstract Scenario en Voorbeelden
Het scenario wordt dan voor iedere rij in de voorbeelden tabel uitgevoerd bijbehorende data.
```
  Functionaliteit: Een beschrijving van de gewenste functionaliteit

  Abstract Scenario: eten
    Stel dat er <start> bananen zijn
    Als ik <min> bananen eet
    Dan zou ik nog <eind> bananen over moeten hebben

  Examples:
    | start | min | eind |
    |  12   |  5  |  7   |
    |  20   |  5  |  15  |

->

  Abstract Scenario: eten
    Stel dat er 12 bananen zijn
    Als ik 5 bananen eet
    Dan zou ik nog 7 bananen over moeten hebben


  Abstract Scenario: eten
    Stel dat er 20 bananen zijn
    Als ik 5 bananen eet
    Dan zou ik nog 15 bananen over moeten hebben

```

De koppeling die momenteel gebruikt wordt om de .feature bestanden te implementeren is Nightwatch. Tests bestaan hiermee dan uit twee delen:
- Het Gherkin .feature deel
- De beschrijving van de test in JavaScript, welke in step_definitions staat.

Hier een voorbeeld van twee bestanden die samen de volledige end-to-end test vormen.

```
# features/google.feature

Feature: Google Search

Scenario: Searching Google

  Gegeven Ik open Google's zoek pagina
  Dan is de titel "Google"
  En het zoek formulier van Google bestaat

```

```
// features/step_definitions/google.js

const {client} = require('nightwatch-cucumber');
const {defineSupportCode} = require('cucumber');

defineSupportCode(({Gegeven: Given, Dan: Then, Als: When}) => {
  Gegeven(/^Ik open Google's zoek pagina$/, () => {
    return client
      .url('http://google.com')
      .waitForElementVisible('body', 1000);
  });

  Dan(/^is de titel ([^"]*)"$/, (title) => {
    return client.assert.title(title);
  });

  Dan(/^het zoek formulier van Google bestaat$/, () => {
    return client.assert.visible('input[name="q"]');
  });

});
```

