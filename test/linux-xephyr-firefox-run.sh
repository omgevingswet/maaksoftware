#!/usr/bin/env bash
set -o errexit
set -o nounset
set -o pipefail

Xephyr :43 &
export DISPLAY=:43

node_modules/.bin/nightwatch -c nightwatch.conf.js -e firefox \
    --skiptags niet_geimplementeerd \
    || node test/html-report.js

# kill most recent background task, in this case Xephyr
kill $!
# or use the process name. %Xephyr is the process id of background Xephyr
# kill %Xephyr
