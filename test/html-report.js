var reporter = require('cucumber-html-reporter');

var options = {
        theme: 'hierarchy',
        jsonFile: 'tests_output/e2e/e2e.json',
        output: 'tests_output/report/cucumber_report.html',
        reportSuiteAsScenarios: true,
        launchReport: true
    };

    reporter.generate(options);
