const { client } = require('nightwatch-cucumber');
const { defineSupportCode } = require('cucumber');

defineSupportCode(({ Given: Gegeven, Then: Dan, When: Als }) => {

    Gegeven('dat de overzichtspagina is geopend', () => {
        // TODO: Check of we daadwerkelijk op de overzichtspagina zitten door een onderscheidende eigenschap
        return client.waitForElementVisible('button', 1000)
            .assert.cssClassPresent('button', 'newDocEditorButton');
    });
});
