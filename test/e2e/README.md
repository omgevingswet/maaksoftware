De e2e tests zijn geimplementeerd met nightwatch. Documentatie over de te gebruiken API staat hier:
[http://nightwatchjs.org/api](http://nightwatchjs.org/api)

Iedere test begint met het openen van de maaksoftware url, en eindigd met het sluiten van de browser sessie.
Daartussen kan code worden aangeroepen vergelijkbaar met de .feature naam.

```javascript
const { client } = require('nightwatch-cucumber');
const { defineSupportCode } = require('cucumber');

defineSupportCode(({ Before: Eerst, Given: Gegeven, Then: Dan, When: Als, After: Laatst }) => {
    Eerst(() => client.url('http://localhost:1234/').pause(1000));

    ...

    Laatst(() => client.end());
});

```

Template plaatsaanduidingen die het .feature bestand met <...> gemarkeerd zijn, zijn in de JavaScript implementatie met {...} te selecteren
Voor de duidelijkheid is ervoor gekozen de aanduiding hetzelfde te noemen als in het .feature bestand,
maar dit is niet noodzakelijk. De {...} selector moet enkel op dezelfde plek staan als de <...> selector in .feature

```javascript
const { client } = require('nightwatch-cucumber');
const { defineSupportCode } = require('cucumber');

defineSupportCode(({ Before: Eerst, Given: Gegeven, Then: Dan, When: Als, After: Laatst }) => {
    Eerst(() => client.url('http://localhost:1234/').pause(1000));

    Gegeven('wat er in de feature {FEATURE} staat', (FEATURE) => {
        client.useXpath();
        client.assert.visible(`//button[contains(., "Open ${BESTANDSNAAM}")]`);

        return client;
    });

    ...

    Dan('iets met een ander {VOORBEELD} uit de voorbeelden tabel', (VOORBEELD) => ...)

    Laatst(() => client.end());
});

```
