const { client } = require('nightwatch-cucumber');
const { defineSupportCode } = require('cucumber');

defineSupportCode(({ Before: Eerst, Given: Gegeven, Then: Dan, When: Als, After: Laatst }) => {
    Eerst(() => client.url('http://localhost:1234/').pause(1000));

   Gegeven('dat de volgende bestanden zichtbaar zijn:', (bestanden) => {
        console.log(bestanden);
   });

    Laatst(() => client.end());
});
