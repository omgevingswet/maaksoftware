const { client } = require('nightwatch-cucumber');
const { defineSupportCode } = require('cucumber');

defineSupportCode(({ Before: Eerst, Given: Gegeven, Then: Dan, When: Als, After: Laatst }) => {
    Eerst(() => client.url('http://localhost:1234/').pause(1000));

    Gegeven('dat het voorbeelddocument genaamd {BESTANDSNAAM} in het overzicht staat', (BESTANDSNAAM) => {
        client.useXpath();
        client.assert.visible(`//button[contains(., "Open ${BESTANDSNAAM}")]`);

        return client;
    });

    Als('de analist op {BESTANDSNAAM} klikt', (BESTANDSNAAM) => {
        client.useXpath();
        client.click(`//button[contains(., "Open ${BESTANDSNAAM}")]`);
        client.useCss();

        return client;
    });

    Dan('wordt de editor geopend', () => {

        client.useCss();
        client.waitForElementVisible('.ProseMirror[contenteditable]', 1000);

        return client;
    });

    Dan('dan staat {TITEL} op de eerste regel', (TITEL) => {

        client.useCss();
        client.expect.element('.ProseMirror[contenteditable]').text.to.contain(TITEL);
        return client;

    });

    Dan('staat de tekstcursor in bewerkbare tekst van het document', () => {

        // document.activeElement

    });

    Laatst(() => client.end());
});
