var path = require('path')

module.exports = {
  entry: './src/app/app.ts',
  resolve: {
    extensions: [".tsx", ".ts", ".js"]
  },
  output: {
    path: path.join(__dirname, 'src', 'static'),
    filename: 'bundle.js',
  },
  devtool: "eval-source-map",
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        loader: 'ts-loader',
        exclude: /node_modules/,
      }
    ],
  },
  plugins: [],
}
