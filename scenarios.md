## Jurist maakt nieuw omgevingsdocument

 * klik op 'nieuw document' en kies 'omgevingsplan'
 * er wordt een interface geopend met een basisstructuur voor het document
   ('aanhef', 'hoofdstuk 1', 'sluiting')
 * jurist schrijft twee artikelen
 * 'voeg werkingsgebied toe'
 * 'opslaan' leidt tot weergave van een verbeelding

### gedetaileerde stappen

1. ga naar http://localhost:1234/
2. klik op 'nieuw opxml'
3. verander documenttype naar 'staatsblad':
   - klik op \<staatscourant>: onderdeel is nu geselecteerd
   - en vervolgens op 'Replace -> staatsblad'

## Jurist past een omgevingsdocument aan

 * klik op een document
 * er wordt een verbeelding weergegeven
 * er is een knop zichtbaar: 'maak een wijzigingsbesluit'
 * document wordt geopend in de editor
 * jurist wijzigt een paragraaf. er is een weergave die aangeeft dat er iets gewijzigd is
 * jurist klikt op 'genereer wijzigingsbesluit'
 * wijzingsbesluit wordt weergegeven

