import {EditorState, Plugin,Transaction} from 'prosemirror-state'
import {menuBar, MenuItem, icons} from 'prosemirror-menu'
import {selectParentNode, toggleMark } from 'prosemirror-commands'
import {redo, undo} from 'prosemirror-history'
import {MaakSchema} from './MaakSchema'
import {EditorView} from 'prosemirror-view'
import {Editor} from "./Editor"
import {saveXML, saveNewXML} from "./save"
import * as helpers from "./helper"

const selectParentNodeItem = new MenuItem({
  css: "",
  class: "",
  execEvent: "mousedown",
  title: "Select parent node",
  run: selectParentNode,
  select: (state) => selectParentNode(state),
  icon: {text: "\u2b1a", css: "font-weight: bold"},
})

function emphasisMenuItem(schema:MaakSchema) {

  const markEm = schema.schema.marks.em
  const cmd = toggleMark(markEm)

  const passedOptions = {
    run: cmd,
    class: "",
    css: "",
    execEvent: "mousedown",
    select: cmd,
    icon: {text: "em", css: "font-style: italic"}
  }

  return new MenuItem(passedOptions)
}

function saveMenuItem(editor: Editor) {

    const saveButton = {
        width: 16,
        height: 16,
        path: "M14 0h-14v16h16v-14l-2-2zM8 2h2v4h-2v-4zM14 14h-12v-12h1v5h9v-5h1.172l0.828 0.828v11.172z"
    }

    const passedOptions = {
        run(_state: EditorState, _dispatch: (p: Transaction) => void, _p2: EditorView, e: Event):void {

            e.preventDefault();

            const xml = editor.toDOM();

            const saveFile = (URI: string): void => {
                saveXML(URI, xml).then(() => {
                    alert(`Opgeslagen: ${URI}`)
                }, (err) => {
                    alert('Kan het document niet opslaan');
                    console.error(err);
                })
            };

            const saveAs = (prompt: string) => {
                const fileName = window.prompt(prompt);

                if (fileName) {
                    const URI = `${editor.baseURI}${editor.schema.schemaName}/${fileName}.xml`;

                    saveNewXML(URI, xml).then(() => {
                        window.location.replace(URI.replace(/\/document(s)?/, '\/editor'));
                        alert(`Opgeslagen: ${fileName}`);
                    }, () => {
                        saveAs(`Bestandsnaam ${fileName} bestaat al, kies een andere`);
                    })

                } else {

                    alert('Kan het document niet opslaan, naam is vereist.');
                }
            };

            if (!editor.saveURI) {

                saveAs('Opslaan als: ');

            } else {

                saveFile(editor.saveURI);
            }
        },
        class: "",
        css: "",
        execEvent: "mousedown",
        icon: saveButton
    };

    return new MenuItem(passedOptions);
}

function toggleView() {

    const cssButton = {
        width: 16,
        height: 16,
        path: "M2.381 0.758l-0.537 2.686h10.934l-0.342 1.735h-10.94l-0.53 2.686h10.933l-0.61 3.063-4.406 1.46-3.819-" +
        "1.46 0.261-1.329h-2.686l-0.639 3.224 6.316 2.417 7.281-2.417 2.403-12.066z"
    }

    const passedOptions = {
        run(_state: EditorState, _fn: (p: Transaction) => void, _view: EditorView, _e: Event):void {

            const editor = document.getElementById('editor') as HTMLDivElement
            if (editor.getAttribute('class') === 'xmlTag') {
                editor.removeAttribute('class')
            } else {
                editor.setAttribute('class', 'xmlTag')
            }
        },
        class: "",
        css: "",
        execEvent: "mousedown",
        icon: cssButton
    }

    return new MenuItem(passedOptions)
}

function treeView() {

    const treeButton = {
        width: 16,
        height: 16,
        path: "M15.25 12h-0.25v-3.25c0-0.965-0.785-1.75-1.75-1.75h-4.25v-2h0.25c0.412 0 0.75-0.338 0.75-0.75v-2.5c0-" +
        "0.413-0.338-0.75-0.75-0.75h-2.5c-0.412 0-0.75 0.337-0.75 0.75v2.5c0 0.412 0.338 0.75 0.75 0.75h0.25v2h-4.25" +
        "c-0.965 0-1.75 0.785-1.75 1.75v3.25h-0.25c-0.412 0-0.75 0.338-0.75 0.75v2.5c0 0.412 0.338 0.75 0.75 0.75h2." +
        "5c0.413 0 0.75-0.338 0.75-0.75v-2.5c0-0.412-0.337-0.75-0.75-0.75h-0.25v-3h4v3h-0.25c-0.412 0-0.75 0.338-" +
        "0.75 0.75v2.5c0 0.412 0.338 0.75 0.75 0.75h2.5c0.412 0 0.75-0.338 0.75-0.75v-2.5c0-0.412-0.338-0.75-0.75-" +
        "0.75h-0.25v-3h4v3h-0.25c-0.412 0-0.75 0.338-0.75 0.75v2.5c0 0.412 0.338 0.75 0.75 0.75h2.5c0.412 0 0.75-" +
        "0.338 0.75-0.75v-2.5c0-0.412-0.338-0.75-0.75-0.75zM3 15h-2v-2h2v2zM9 15h-2v-2h2v2zM7 4v-2h2v2h-2zM15 15h-2v" +
        "-2h2v2z"
    }

    const passedOptions = {
        run(_state: EditorState, _fn: (p: Transaction) => void, _view: EditorView, _e: Event):void {

            // show hide div 'hierarchicalTreeDiv'
            const hierarchicalTreeDiv = document.getElementById('hierarchicalTreeDiv') as HTMLDivElement

            // remove existing tree first
            const existingTreeArray = document.querySelector('ul.tree') as HTMLDivElement
            if (hierarchicalTreeDiv.contains(existingTreeArray)) {
                hierarchicalTreeDiv.removeChild(existingTreeArray)
            }

            if (hierarchicalTreeDiv.style.display === 'none' || hierarchicalTreeDiv.style.display === '') {

                // get tree array from HTML dom
                const treeArray = getTreeArray()
                // create main hierchicalTree element
                let hierarchicalTree = document.createElement("ul")
                hierarchicalTree.className = 'tree'

                // put all elements from tree array to hierarchicalTree element
                hierarchicalTree = getHierarchalTreeFromTreeArray(treeArray, hierarchicalTree)

                // append hierarchicalTree to hierarchicalTreeDiv
                hierarchicalTreeDiv.appendChild(hierarchicalTree)
                hierarchicalTreeDiv.style.display = 'block';

            } else {
                hierarchicalTreeDiv.style.display = 'none';
            }
        },
        class: "",
        css: "",
        execEvent: "mousedown",
        icon: treeButton
    }

    return new MenuItem(passedOptions)
}

function getHierarchalTreeFromTreeArray(treeNode:any[], hierarchicalTree:HTMLUListElement):HTMLUListElement {

    for(const leaf of treeNode) {

        // get data from treeArray
        const idDataArr = (leaf.value.Id).split("|||")

        // create leaf
        const liLeaf = document.createElement("li") as HTMLLIElement
        liLeaf.innerHTML = '<a href="#" ' +
                'onclick="var offsetTop = document.evaluate(\'//span[text() =\\\''+ idDataArr[1]+'\\\']\', ' +
                    'document, null, XPathResult.ANY_TYPE, null).iterateNext().offsetTop; ' +
                    'document.getElementsByClassName(\'ProseMirror\')[0].scrollTop = offsetTop - 15">'
                + helpers.capitalizeFirstLetter(idDataArr[0]) + ' ' + idDataArr[1] + ' - ' + idDataArr[2]
            + '</a>'

        hierarchicalTree.appendChild(liLeaf)

        // add children
        if ("children" in leaf && leaf.children.length > 0) {
            const childUlElement = document.createElement("ul") as HTMLUListElement
            hierarchicalTree.appendChild(childUlElement)

            // do the magic
            getHierarchalTreeFromTreeArray(leaf.children, childUlElement)
        }
    }

    return hierarchicalTree
}

function getTreeArray():any[] {

    const list = document.querySelectorAll('*[data-tag=kop]')

    const kopItems:any[] = []
    for (let i = 0; i < list.length; ++i) {

        // get childnodes of kop
        const childNodes = Array.prototype.slice.call(list.item(i).childNodes)

        const titel = childNodes.filter(function(item:any) {
            return item.getAttribute('data-tag') === 'titel'
        });

        const label = childNodes.filter(function(item:any) {
            return item.getAttribute('data-tag') === 'label'
        });

        const nr = childNodes.filter(function(item:any) {
            return item.getAttribute('data-tag') === 'nr'
        });

        if (label.length > 0 && nr.length > 0 && titel.length > 0) {
            kopItems.push({
                "Id": label[0].textContent + "|||" + nr[0].textContent + "|||" + titel[0].textContent,
                "Name": nr[0].textContent,
                "Parent": getChildIdOfItem(list.item(i))
            })
        }
    }

    return buildHierarchy(kopItems)
}

function getChildIdOfItem(item:any):string {
    const childNodesOfParent = item.parentNode.parentNode.childNodes;

    for (const childNodeOfParent of childNodesOfParent) {

        if (childNodeOfParent.getAttribute('data-tag') === 'kop') {
            const childNodesOfKop = Array.prototype.slice.call(childNodeOfParent.childNodes);

            const childTitel = childNodesOfKop.filter(function(item:any) {
                return item.getAttribute('data-tag') === 'titel';
            });

            const childNr = childNodesOfKop.filter(function(item:any) {
                return item.getAttribute('data-tag') === 'nr';
            });

            const childLabel = childNodesOfKop.filter(function(item:any) {
                return item.getAttribute('data-tag') === 'label';
            });

            return childLabel[0].textContent + "|||" + childNr[0].textContent + "|||" + childTitel[0].textContent
        }
    }
    return ''
}

function buildHierarchy(arry:any[]) {

    const roots:any[] = []
    const children:any[] = [];

    // find the top level nodes and hash the children based on parent
    for (let i = 0, len = arry.length; i < len; ++i) {
        const item = arry[i]
        const p = item.Parent
        const target = !p ? roots : (children[p] || (children[p] = []));

        target.push({ value: item });
    }

    // function to recursively build the tree
    const findChildren = function(parent:any) {
        if (children[parent.value.Id]) {
            parent.children = children[parent.value.Id];
            for (let i = 0, len = parent.children.length; i < len; ++i) {
                findChildren(parent.children[i]);
            }
        }
    };

    // enumerate through to handle the case where there are multiple roots
    for (let i = 0, len = roots.length; i < len; ++i) {
        findChildren(roots[i]);
    }

    return roots;
}

function closeButton() {

    const closeButton = {
        width: 16,
        height: 16,
        path: "M15.854 12.854c-0-0-0-0-0-0l-4.854-4.854 4.854-4.854c0-0 0-0 0-0 0.052-0.052" +
        " 0.090-0.113 0.114-0.178 0.066-0.178 0.028-0.386-0.114-0.529l-2.293-2.293c-0.143-0" +
        ".143-0.351-0.181-0.529-0.114-0.065 0.024-0.126 0.062-0.178 0.114 0 0-0 0-0 0l-4.854" +
        " 4.854-4.854-4.854c-0-0-0-0-0-0-0.052-0.052-0.113-0.090-0.178-0.114-0.178-0.066-0.386" +
        "-0.029-0.529 0.114l-2.293 2.293c-0.143 0.143-0.181 0.351-0.114 0.529 0.024 0.065 0.062" +
        " 0.126 0.114 0.178 0 0 0 0 0 0l4.854 4.854-4.854 4.854c-0 0-0 0-0 0-0.052 0.052-0.090" +
        " 0.113-0.114 0.178-0.066 0.178-0.029 0.386 0.114 0.529l2.293 2.293c0.143 0.143 0.351" +
        " 0.181 0.529 0.114 0.065-0.024 0.126-0.062 0.178-0.114 0-0 0-0 0-0l4.854-4.854 4.854" +
        " 4.854c0 0 0 0 0 0 0.052 0.052 0.113 0.090 0.178 0.114 0.178 0.066 0.386 0.029" +
        " 0.529-0.114l2.293-2.293c0.143-0.143 0.181-0.351 0.114-0.529-0.024-0.065-0.062-0.126-0.114-0.178z"
    }

    const passedOptions = {
        run(_p1: EditorState, _fn: (p: Transaction) => void, view: EditorView, e: Event):void {
            e.preventDefault();
            view.destroy();

            // Reroute app to index page after editor is closed
            location.assign('/');
        },
        class: "",
        css: "",
        execEvent: "mousedown",
        icon: closeButton
    }

    return new MenuItem(passedOptions)
}

export const createMenu = (schema: MaakSchema, editor:Editor): Plugin => {
  const content = [
    [
        closeButton(),
      new MenuItem({
        title: "Undo last change",
        class: "",
        css: "",
        execEvent: "mousedown",
        run: undo,
        select: undo,
        icon: icons.undo
      }),
      new MenuItem({
        title: "Redo last change",
        class: "",
        css: "",
        execEvent: "mousedown",
        run: redo,
        select: redo,
        icon: icons.redo
      }),
      selectParentNodeItem,
      emphasisMenuItem(schema),
        saveMenuItem(editor),
        toggleView(),
        treeView()
    ]
  ]

  const menu = menuBar({
    floating: false,
    content
  })
  return menu
}
