import {Schema} from 'prosemirror-model'

// :: Object
//
// Node definitions with `htmlTag` instead of `parseDOM` and `toDOM` for some
// node definitions. Missing `parseDOM` and `toDOM are derived from `tag` and
// `htmlTag` by `tagMappingToSchemaNodes`.
const tagMapping = {
  toestand: {
    content: "metadata regeling locatieAanduidingen?",
    defining: true,
    htmlTag: "article"
  },
  metadata: {
    atom: true,
    htmlTag: "div"
  },
  regeling: {
    content: "opschrift? aanhef? regelingTekst",
    defining: true,
    htmlTag: "section"
  },
  locatieAanduidingen: {
    tag: "locatie-aanduidingen",
    defining: true,
    htmlTag: "div"
  },
  opschrift: {
    content: "al+",
    htmlTag: "section"
  },
  aanhef: {
    content: "al+",
    htmlTag: "section"
  },
  regelingTekst: {
    tag: "regeling-tekst",
    content: "hoofdstuk+",
    htmlTag: "section",
  },
  hoofdstuk: {
    content: "kop artikel+",
    htmlTag: "section"
  },
  artikel: {
    content: "kop al+",
    htmlTag: "section"
  },
  kop: {
    content: "titel",
    htmlTag: "header"
  },
  titel: {
    content: "inline<em>*",
    htmlTag: "h1"
  },
  al: {
    content: "inline<_>*",
    htmlTag: "p"
  }
};

const tagMappingToSchemaNodes = (tagMapping, schemaNodes) => {
  Object.keys(tagMapping).forEach((key) => {
    const def = tagMapping[key];
    const htmlTag = def.htmlTag;
    const tag = def.tag || key;
    if (!def.parseDOM) {
      def.parseDOM = [{tag: htmlTag + "[data-tag='" + tag + "']"}]
    }
    if (!def.toDOM) {
      const toDOM = [htmlTag, {"data-tag": tag}];
      if (def.content) {
        toDOM.push(0);
      }
      def.toDOM = () => { return toDOM };
    }
    schemaNodes[key] = def;
  });
}

const nodes = {
  doc: {
    content: "toestand"
  },
  text: {
    group: "inline"
  },
  wow: {
    inline: true,
    content: "inline<_>*",
    parseDOM: [{tag: "wow"}],
    toDOM() { return ["wow", 0] },
    group: "inline"
  },
}

tagMappingToSchemaNodes(tagMapping, nodes);

exports.nodes = nodes

// :: Object
//
//  link:: MarkSpec A link. Has `href` and `title` attributes.
//  `title` defaults to the empty string.
//
//  em:: MarkSpec An emphasis mark.
//
//  strong:: MarkSpec A strong mark.
//
//  code:: MarkSpec Code font mark.
const marks = {
  link: {
    attrs: {
      href: {},
      title: {default: null}
    },
    inclusive: false,
    parseDOM: [{tag: "a[href]", getAttrs(dom) {
      return {href: dom.getAttribute("href"), title: dom.getAttribute("title")}
    }}],
    toDOM(node) { return ["a", node.attrs] }
  },
  em: {
    parseDOM: [{tag: "em"}],
    toDOM() { return ["em"] }
  },
  strong: {
    parseDOM: [{tag: "strong"}],
    toDOM() { return ["strong"] }
  },
  code: {
    parseDOM: [{tag: "code"}],
    toDOM() { return ["code"] }
  }
}
exports.marks = marks

const schema = new Schema({nodes, marks})
exports.schema = schema
