export const saveXML = (path: string | null, doc: Document): Promise<Number> => {
    return new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest();

        if (path) {
            xhr.overrideMimeType('application/xml');
            xhr.open('PUT', path, true)
            xhr.onreadystatechange = () => {
                if (xhr.readyState === 4) {
                    if (xhr.status >= 200 && xhr.status < 300) {
                        resolve(xhr.status);
                    } else {
                        reject(xhr.status);
                    }
                }
            };
            xhr.send(doc);
        } else {
            reject('Het document kan niet worden opgeslagen. Geen URL voor opslaan gespecificeerd');
        }
    });
};

/*
    Function to check if newFile path already exists.
    If so, we will not overwrite this existing file but reject the save attempt
*/
export const saveNewXML = (path: string, doc: Document): Promise<Number> => {
    return new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest();
        xhr.open('HEAD', path);
        xhr.onreadystatechange = () => {

            if (xhr.status === 404) {

                saveXML(path, doc).then(resolve, reject);

            } else if (xhr.readyState === xhr.DONE) {

                reject();
            }
        };
        xhr.send();
    })
}
