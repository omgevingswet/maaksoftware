import { expect } from 'chai'
import * as helpers from './helper'

describe('helper', () => {
    it('should has a function that remove the last token from word', () => {
        expect(helpers.removeLastTokenFromWord('dit_is_een_test', '_')).to.equal('dit_is_een')
    })
    it('should has a function that capitalize the first letter', () => {
        expect(helpers.capitalizeFirstLetter('abc')).to.equal('Abc')
    })
})
