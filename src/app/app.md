# App bestaat uit de volgende onderdelen:
- Overzicht van mogelijke schemas (TPOD's)
  - Voor ieder schema een knop voor het maken van een nieuw document
  - Voor ieder schema een overzicht van alle documenten die al met dit schema gemaakt zijn. Dit overzicht bestaat uit:
    - `toestanden`
      - Voor iedere toestand bestaat de mogelijkheid deze te openen, te downloaden, een nieuwe versie te starten
    - `besluiten`
      - Voor ieder besluit bestaat de mogelijkheid deze te downloaden.

- Wanneer een nieuw document wordt gemaakt
  - Wordt het bijbehorende JSON schema geladen
  - Wordt een template opgehaald, wanneer beschikbaar
  - Wordt de auteur gevraagd om de bestandsnaam te specificeren
  - Wordt de Prosemirror editor opgestart
    - Met het beschikbare schema dat is opgebouwd uit het basisschema, schemas voor metadata en annotaties en de geodata
    - Met de beschikbare informatie over root bestanden (is dit nodig of staat dit in het gegenereerde JSON schema?)
    - Wanneer er een template beschikbaar is wordt deze ingeladen
    - Wanneer er geen template beschikbaar is wordt het basis root element opgehaald
      - (Een uiteindelijk schema zou dus maar 1 mogelijke root moeten hebben, of een opgeslagen voorkeurs root)

- Wanneer een document wordt geopend
  - Wordt het bijbehorende JSON schema geladen
  - Wordt het document opgehaald
  - Wordt de Prosemirror editor opgestart
    - Met het beschikbare schema
    - Met het opgehaalde document

- Bij het opslaan
  - Wordt de bestandsnaam automatisch opnieuw opgebouwd titel-versie-datum-volgnummer
  - Wordt het bestand opgeslagen met de nieuwe bestandsnaam
  - Wordt het algemene bestand met titel-versie overschreven met deze nieuwste versie

- Bij het opslaan als nieuwe versie
  - Wordt de nieuwe versie nummer huidige-versie + 1
  - Wordt de bestandsnaam opnieuw opgebouwd titel-nieuweVersie-datum-1
  - Wordt het bestand opgeslagen met de nieuwe bestandsnaam
  - Wordt een algemeen bestand gecreeerd als titel-nieuweVersie, met een kopie van het huidige bestand
