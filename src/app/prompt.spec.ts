import { expect } from 'chai'
import { MaakSchema, MaakSchemaSpec } from './MaakSchema'
import { JSDOM } from 'jsdom'
import {
  openDialog, addError, addPatternError, hideErrors, validateFieldRequired, validateFieldPattern,
  openAttributesDialog
} from './prompt'
import {EditorState} from 'prosemirror-state';
import {transactionOnNode} from "./HierarchyMenu";
// import { transactionOnNode } from './HierarchyMenu'

const newDocument = (): { jsdom: JSDOM; document: Document } => {
  const jsdom = new JSDOM()     // dummy dom
  return {jsdom, document: jsdom.window.document}
}

// const newMaakSchema = (schema: MaakSchemaSpec, root: string) => {
//   return ((schema: MaakSchemaSpec, root: string) => {
//     return new MaakSchema(schema, [root])
//   }).bind(null, schema, root)
// }
const jsonSchema: MaakSchemaSpec = {
  "root": {
    tag: "root",
        htmlTag: "div",
        inline: false,
        mark: false,
        any: false,
        root: true,
        namespace: null,
        content: "text*",
        attrs: [
          {"id": {
            "tag": "id",
            "required": false,
            "type": "string",
            "default": null
          }}
        ]
  }
}

describe('In a prompt', () => {
  const jsdom = new JSDOM()

  it('a function openDialog should be work with simple parameters', () => {
    const htmlElement = jsdom.window.document.createElement('div')
    expect(openDialog(htmlElement, function() { return true; } )).to.equal(true)
  })
  it('function openAttributesDialog should return a decent HTMLElement', () => {
    const mySchema = new MaakSchema(jsonSchema, ["root"])
    // ik ga met doc nieuwe elementen maken, en kijk dan of ik kan runnen met schema
    const {document: doc} = newDocument();
    const root = doc.createElementNS(null, 'root')
    const node = mySchema.fromDOM(root)

    const myState = EditorState.create({ schema: mySchema.schema })
    const trans = transactionOnNode(1, myState)

    try {
        openAttributesDialog(myState, trans, mySchema, node)
    } catch (_e) {
        /** disabled because it is broken */
    }

    // openAttributesDialog. how to mock Editorstate, dispatch and transaction on node?
  })
  it('function getNodeOrMarkFromSelection should return a node (or mark) from selection', () => {
    // todo. how to mock editorstate and dispatch
  })
  it('function getAttributeValueWithinSelection should return the attribute value within selection', () => {
    // todo. how to mock editorstate and dispatch
  })
  it('function addError should have the same HTMLElement as the HTMLElement from the input', () => {
    const errorField = jsdom.window.document.createElement('div')
    errorField.innerHTML = 'hoi'
    const errorFieldFromMethod = addError(errorField, 'hoi')

    expect(errorFieldFromMethod.innerHTML).to.be.equal(errorFieldFromMethod.innerHTML)
  })
  it('function addPatternError should show a good error message', () => {
    const errorField = jsdom.window.document.createElement('div')
    const errorFieldFromMethod = addPatternError(errorField, 'inputField')
    expect(errorFieldFromMethod.innerHTML).to.be.equal('De volgende velden kloppen niet: inputField, ')
  })
  it('function hideErrors should hide the divs', () => {
     const errorField = jsdom.window.document.createElement('div')
     errorField.id = 'errorField'

     const errorFieldsFromMethod = hideErrors([errorField])

     for (const errorFieldFromMethod of errorFieldsFromMethod) {
       expect(errorFieldFromMethod.style.display).to.be.equal('none')
       expect(errorFieldFromMethod.innerHTML).to.be.equal('')
     }
  })
  it('function validateFieldRequired should return false when a field is required', () => {
    const errorField = jsdom.window.document.createElement('div')
    const dummyObject = { required: true }
    const returnValue = validateFieldRequired(errorField, dummyObject, '')
    expect(returnValue).to.be.equal(false)
  })
  it('function validateFieldRequired should return true when a field is NOT required', () => {
    const errorField = jsdom.window.document.createElement('div')
    const dummyObject = { required: false }
    const returnValue = validateFieldRequired(errorField, dummyObject, '')
    expect(returnValue).to.be.equal(true)
  })
  it('function validateFieldPattern should return false when a fieldValue does not meet the pattern', () => {
    const errorField = jsdom.window.document.createElement('div')
    const dummyObject = { type: 'string', pattern: '\\w+', required: false }
    const stringToBeChecked = ''
    const returnValue = validateFieldPattern(errorField, dummyObject, stringToBeChecked, 'id')
    expect(returnValue).to.be.equal(false)
  })
  it('function validateFieldPattern should return true when a fieldValue DOES MEET the pattern', () => {
    const errorField = jsdom.window.document.createElement('div')
    const dummyObject = { type: 'string', pattern: '\\w+', required: false }
    const stringToBeChecked = 'abc'
    const returnValue = validateFieldPattern(errorField, dummyObject, stringToBeChecked, 'id')
    expect(returnValue).to.be.equal(true)
  })
})
