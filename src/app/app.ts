import {
    MaakSchema,
    MaakSchemaSpec
} from './MaakSchema'; // Todo move EditorInstance to server and remove MaakSchema dependency

import { Editor } from './Editor';
import * as _ from 'lodash';

// TODO: Move Editor logic to router callback of '/edit/...'
class EditorInstance {
    editor: Editor | null;

    constructor(parent: HTMLDivElement, schema?: MaakSchema, doc?: Document, saveURI?: string) {
        // Remove all content from the parent
        while (parent.firstChild) {
            parent.removeChild(parent.firstChild);
        }

        this.editor = schema ? new Editor(parent, schema, doc, saveURI) : null;
    }
}

const passError = (error: Error) => { throw error };

/*
    Use fetch api to get the schemas provided by the router
    Returns a Promise with a json object
*/
const fetchJSON = (path: string): Promise<Response> => {
  return fetch(path).then((response) => response.json(), passError);
};

const fetchXML = (path: string): Promise<Document> => {
    return new Promise((resolve, reject) => {
        if (path) {
            const xhr = new XMLHttpRequest();

            xhr.open('GET', path, true)
            xhr.onreadystatechange = () => {
                if (xhr.readyState === 4) {
                    if (xhr.responseXML) {
                        resolve(xhr.responseXML);
                    } else {
                        reject(xhr.responseText);
                    }
                }
            };

            xhr.send(null);

        } else {
            resolve();
        }
    });
};

const deterimineRootNodes = (schema: MaakSchemaSpec, doc: Document, _schemaName: string = ''): string[] => {
    let roots = _.keys(_.pickBy(schema, (el) => el.root && !el.inline));
    let testRoots: string[];

    if (doc) {
        testRoots = roots.filter((root) => {
            const rootName = root.replace(/_\d+$/, '');
            return rootName === doc.documentElement.localName;
        });
    } else {
        testRoots = roots.filter((root) => {
            const rootName = root.replace(/_\d+$/, '');

            return rootName === 'toestand' // Use 'toestand' by default
                || /_1$/.test(root) // If root number one exists, use this one
        })
    }

    roots = testRoots.length > 0 ? testRoots : roots;

    return roots;
};

const initEditorPage = ():void => {
    const editorEl = document.querySelector('div[data-document][data-schema]') as HTMLDivElement;

    if (editorEl) {
        const schemaURI = editorEl.getAttribute('data-schema') || '';
        const documentURI = editorEl.getAttribute('data-document') || '';

        Promise.all([
            fetchJSON(schemaURI),
            fetchXML(documentURI)
        ]).then((result) => {
            const [schemaSpec, doc] = result as any;
            const schemaName = schemaURI.replace(/^.+\/|\.json$/g, '');
            const roots = deterimineRootNodes(schemaSpec as MaakSchemaSpec, doc, schemaName);
            const schema = new MaakSchema(schemaSpec as MaakSchemaSpec, roots, schemaName);

            return new EditorInstance(editorEl, schema, doc, documentURI);
        });
    }
};

const init = () => {
    // Check which type of page this script is executed on.
    // The convention is that the `id` attribute on the <body> element
    // indicates what application code to load.
    // Eventually this has to be updated to use client side URL routing.

    if (document.getElementById('editor-page')) {
        initEditorPage();
    }
};

document.addEventListener('DOMContentLoaded', init, false);
