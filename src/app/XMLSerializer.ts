import { Namespaces } from './Namespaces'

/* shim for XMLSerializer to use in tests with JSDOM */

const serializeName = (xml: string[], ns: Namespaces, e: Element) => {
    if (e.namespaceURI) {
        const prefix = e.prefix || ""
        xml.push(e.tagName);
        if (ns.resolve(prefix) !== e.namespaceURI) {
            if (prefix.length) {
                xml.push(" xmlns:", prefix, "='", e.namespaceURI, "'")
            } else {
                xml.push(" xmlns='", e.namespaceURI, "'")
            }
            ns.insert(prefix, e.namespaceURI)
        }
    } else {
        xml.push(e.localName || "")
    }
}

const serializeElement = (xml: string[], ns: Namespaces, e: Element) => {
    xml.push("<")
    serializeName(xml, ns, e)
    if (e.childNodes.length) {
        xml.push(">")
        const cns = ns.nest()
        e.childNodes.forEach((child) => {
            if (child.nodeType === 1) {
                serializeElement(xml, cns, child as Element)
            }
        })
        xml.push("</")
        serializeName(xml, ns, e)
        xml.push(">")
    } else {
        xml.push("/>")
    }
}

export class XMLSerializer {
    serializeToString(node: Node): string {
        const xml: string[] = [];
        const ns = new Namespaces();
        if (node.nodeType === 1) {
            serializeElement(xml, ns, node as Element)
        } else if (node.nodeType === 9) {
            serializeElement(xml, ns, (node as Document).documentElement)
        }
        return xml.join("")
    }
}
