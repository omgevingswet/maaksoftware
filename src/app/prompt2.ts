import {EditorState, NodeSelection} from "prosemirror-state";
import {Node, Mark} from "prosemirror-model";
import * as ol from "openlayers";

const okHandler = () => {
/*
	// var these = this;
	var groep = [];
	var locatieId = 0;
	var hoofdwerkingsgebied = {};
//	var coordinaten = [];
	var pointCoordinates = [];
	var lineCoordinates = [];
	var polygonCoordinates = [];

	// GET COORDS TO SAVE
	map.getLayers().forEach(function(layer) {
	    if (layer.layer_type === 'vector') {

	        var features = layer.getSource().getFeatures();
	        var _polygonCoordinates = [];

	        features.forEach(function(feature) {
	            var coords = feature.getGeometry().getCoordinates();

	            switch(coords.length) {
	                case 2:
	                    switch(isArray(coords[0])) {
	                        case true:
	                            // first element is array, so it is a line
	                            var _linePoints = "";
	                            coords.forEach(function(coordLine) {
									coordLine = transformToEtrs89(coordLine);
	                                _linePoints += " " + coordLine[0]+","+coordLine[1];
	                            });

	                            lineCoordinates.push({
	                                'gml:coordinates': _linePoints.trim()
	                            });

	                            break;
	                        case false:
	                            // first element is not array, so it is a point
								coords = transformToEtrs89(coords);
	                            pointCoordinates.push({
	                                'gml:coordinates': coords[0]+","+coords[1]
	                            });
	                            break;
	                    }
	                    break;
	                default:
	                    // else, polygon
	                    var _polygonPoints = "";
	                    //var _polygonCoordinates = [];
	                    coords.forEach(function(coordLine) {
	                        coordLine.forEach(function (coordPoint) {
								coordPoint = transformToEtrs89(coordPoint);
	                            _polygonPoints += " " + coordPoint[0]+","+coordPoint[1];
	                        });
	                    });

	                    if (_polygonPoints != "") {
	                        _polygonCoordinates.push({
	                            'gml:coordinates': _polygonPoints.trim()
	                        });
	                    }
	                    break;
	            }
	        });

	        if (_polygonCoordinates.length > 0) {
	            polygonCoordinates.push({
	                'gml:outerBoundaryIs': {
	                    'gml:LinearRing': _polygonCoordinates
	                }
	            });
	        }

	        if (
	            pointCoordinates.length > 0
	            || lineCoordinates.length > 0
	            || polygonCoordinates.length > 0) {

	            groep.push({
	                '$': {
	                    'id': locatieId,
	                    'idref': 0
	                },
	                'geometrie': {
	                    '$': {
	                        'id': locatieId,
	                        'idref': 0,
	                        'idealisatie': 0,
	                        'type': 'geo'
	                    },
	                    'feature:geometry': {
	                        'gml:Point': pointCoordinates,
	                        'gml:LineString': lineCoordinates,
	                        'gml:Polygon': polygonCoordinates
	                    }
	                }
	            });

	            hoofdwerkingsgebied = {
	                '$': {
	                    'ref': locatieId
	                }
	            };
	        }
	    }
	});
*/
    // SHOW RESULT
    // console.log("groep", groep);
}

const loadGML = (gml: string) => {
    const io = new ol.format.GML3({featureNS:"urn:a",srsName:"http://www.opengis.net/def/crs/EPSG/0/4326"})
    const parser = new DOMParser();
    const dom = parser.parseFromString(gml, "application/xml")
    let c = dom.documentElement.firstElementChild
    while (c) {
        const features = io.readFeatures(c, {
    featureProjection: 'EPSG:28992',
    dataProjection: 'EPSG:4326'
  })
console.log(c)
console.log(features)
        c = c.nextElementSibling
    }
    console.log(io.readFeatures(dom))
    console.log(io.readFeatures(gml))
}

export const openDialog = (gml: string):boolean => {
    loadGML(gml)

    const br = document.createElement("br")

    // container (main)
    const modal = document.body.appendChild(document.createElement('div'))
    modal.className = 'modal'

    let container = document.querySelector('.modalContainer') as HTMLDivElement
    if (container) {
        container.remove()
    } else {
        container = document.createElement('div');
        container.className = 'modalContainer'

        modal.appendChild(container)
    }
    container.style.top = '5%';
    container.style.width = '90%';
    container.style.height = '70%';

    // label of dialog
    const label = document.createElement("text")
    label.innerHTML = 'Bewerk bewerkingsgebied'
    label.className = 'dialogHeader'

    container.appendChild(label)
    container.appendChild(br.cloneNode())
    container.appendChild(br.cloneNode())
    const mapDiv = document.createElement("div")
    mapDiv.id = 'map'
    container.appendChild(mapDiv)

    // fields (attrs) of dialog
    const noAttributes:boolean = false

    container.appendChild(br.cloneNode())

    // cancel button
    if (noAttributes === false) {
        const cancelButton = document.createElement("button")
        cancelButton.id = 'cancelButton'
        cancelButton.innerHTML = 'Cancel'
        cancelButton.addEventListener('click', (e) => {
            e.preventDefault()
            modal.remove();
        })
        container.appendChild(cancelButton)
    }

    // ok button
    const okButton = document.createElement("button")
    okButton.id = 'okButton'
    okButton.innerHTML = 'OK'
    okButton.addEventListener('click', (e) => {
        e.preventDefault()
        hideError()
        okHandler()
        modal.remove();
    })
    container.appendChild(okButton)

    const errorField = document.createElement("div")
    errorField.id = 'errorField'
    errorField.className = 'errorField'
    container.appendChild(errorField)

    const patternErrorField = document.createElement("div")
    patternErrorField.id = 'patternErrorField'
    patternErrorField.className = 'patternErrorField'
    container.appendChild(patternErrorField)

    const map = new ol.Map({
        layers: [
            new ol.layer.Tile({
                source: new ol.source.OSM()
            })/*,
            new ol.layer.Vector({
                source: new ol.source.Vector({
                    url: "/test.gml"
                })
            })*/
        ],
        view: new ol.View({
            center: [0, 0],
            zoom: 2
        }),
        target: 'map'
    })

    return map !== null
}

const hideError = ():void => {
    const errorField = document.getElementById('errorField') as HTMLDivElement
    errorField.style.display = 'none'
    errorField.innerHTML = ''

    const patternErrorField = document.getElementById('patternErrorField') as HTMLDivElement
    patternErrorField.style.display = 'none'
    patternErrorField.innerHTML = ''
}

export const getNodeOrMarkFromSelection = (state:EditorState):undefined|Node|Mark => {
    const ref = state.selection as NodeSelection
    if (ref.from !== ref.to) {
        const nodeSelection = new NodeSelection(ref.$from)

        // test if mark available first
        if (
            nodeSelection.node.marks !== undefined && nodeSelection.node.marks.length > 0) {
            return nodeSelection.node.marks[0]

        } else if (nodeSelection.node !== undefined) {
            return nodeSelection.node
        }
    }
    return undefined
}
