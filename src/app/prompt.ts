import {EditorState, NodeSelection, Transaction} from "prosemirror-state";
import {NodeType, Node, Mark} from "prosemirror-model";
import {MaakSchema} from './MaakSchema'
import * as model from 'prosemirror-model'
import * as helper from './helper'

interface IAttributeSpecNode {
    required: boolean,
    tag: string,
    description: string,
    type: string,
    default: string,
    pattern: string | any[]
}

export const openDialog = (
    body:HTMLElement, callback: (ok: boolean) => void
):boolean => {
    const doc = body.ownerDocument
    const parent = doc.body
    // add body to modal
    const modal = parent.appendChild(doc.createElement('div'))
    modal.className = 'modal'

    let container = doc.querySelector('.modalContainer') as HTMLDivElement
    if (container) {
        container.remove()
    } else {
        container = doc.createElement('div');
        container.className = 'modalContainer'

        modal.appendChild(container)
    }

    const ok = doc.createElement('button') as HTMLButtonElement
    ok.innerHTML = 'OK'
    ok.onclick = () => {
        callback(true)
        modal.remove()
    }
    const cancel = doc.createElement('button') as HTMLButtonElement
    cancel.innerHTML = 'Cancel'
    cancel.onclick = () => {
        callback(false)
        modal.remove()
    }
    container.appendChild(body)
    container.appendChild(cancel)
    container.appendChild(ok)

    return true
}

export const openAttributesDialog = (
    state: EditorState,
    transactionOnNode: Transaction,
    schema: MaakSchema,
    node: model.Node) => {

    const bodyContainer = document.createElement('div')
    const bodyAttributeDialog = getBodyOfAttributeDialog(bodyContainer, state, transactionOnNode, schema, node)

    openDialog(bodyAttributeDialog, (bool:boolean) => {

        if (bool) {
            getOKFunctionOfAttributeDialog(transactionOnNode, state, schema, node)
        } else {
            // bool = false. do nothing here?
        }
    })
}

export const getBodyOfAttributeDialog = (
    bodyContainer: HTMLDivElement,
    state: EditorState,
    transactionOnNode: Transaction,
    schema: MaakSchema,
    node: model.Node,
    ):HTMLElement => {

    const jsonSchemaNode                    = schema.jsonSchema[node.type.name]
    const attrs:any[]|undefined             = jsonSchemaNode.attrs
    const isMark:boolean                    = jsonSchemaNode.mark
    const titleOfDialog = "Bewerk attribuut '" + helper.removeLastTokenFromWord(node.type.name, '_') + "'"
    const br = document.createElement('br')

    const errorField = document.getElementById('errorField') as HTMLDivElement
    const patternErrorField = document.getElementById('patternErrorField') as HTMLDivElement
    hideErrors([errorField, patternErrorField])

    // add title of dialog
    const label = document.createElement("text")
    label.innerHTML = titleOfDialog
    label.className = 'dialogHeader'
    bodyContainer.appendChild(label)
    bodyContainer.appendChild(br.cloneNode())
    bodyContainer.appendChild(br.cloneNode())

    if (typeof(attrs) === 'undefined' || (typeof(attrs) !== 'undefined' && attrs.length === 0)) {
        // no attrs, so show 'no attrs screen'
        const fieldLabel = document.createElement("text")
        fieldLabel.innerHTML = 'Dit element bevat geen attributen!'
        bodyContainer.appendChild(fieldLabel)
        bodyContainer.appendChild(br.cloneNode())

    } else if (isMark) {
        // is mark, so not supported yet
        const fieldLabel = document.createElement("text")
        fieldLabel.innerHTML = 'Dit element is van het type "Mark", ' +
            'en van dit type kunnen de attributen nog niet worden gewijzigd!'
        bodyContainer.appendChild(fieldLabel)
        bodyContainer.appendChild(br.cloneNode())

    } else {
        // is node
        for (const field of attrs) {
            const fieldId:string = Object.keys(field)[0]
            const fieldObject:IAttributeSpecNode = field[fieldId]

            const fieldLabel = document.createElement("text")
            fieldLabel.innerHTML = fieldObject.tag +
                ( ("required" in fieldObject) && fieldObject.required ? " <span class ='req'>*</span>" : "")
            fieldLabel.className = 'fieldLabel'
            fieldLabel.title = fieldObject.description !== undefined ? fieldObject.description : ''

            let inputField:HTMLElement = document.createElement('input')
            const valueOfInput = getAttributeValueWithinSelection(state, fieldId, transactionOnNode)

            if (fieldObject.type === 'string') {
                inputField.id = fieldId.replace(":", "");
                inputField.className = 'field';
                (inputField as HTMLInputElement).required =
                    (("required" in fieldObject) && fieldObject.required ? true : false);
                (inputField as HTMLInputElement).value =
                    (( (valueOfInput === null || valueOfInput.trim() === '') && fieldObject.default !== '') ?
                        fieldObject.default : valueOfInput);

            } else if (fieldObject.type === 'enumeration' && "pattern" in fieldObject) {
                inputField = document.createElement('select');
                inputField.id = fieldId.replace(":", "");
                inputField.className = 'field';
                (inputField as HTMLSelectElement).required =
                    (("required" in fieldObject) && fieldObject.required ? true : false);

                for (const optionValue of (fieldObject.pattern as string).split('|')) {
                    const option = document.createElement("option") as HTMLOptionElement;
                    option.text = optionValue.trim();

                    if (typeof(valueOfInput) !== 'undefined' && option.text === valueOfInput) {
                        option.selected = true;
                    }
                    (inputField as HTMLSelectElement).add(option);
                }
            }
            bodyContainer.appendChild(fieldLabel)
            bodyContainer.appendChild(inputField)
            bodyContainer.appendChild(br.cloneNode())

            const errorField = document.createElement("div")
            errorField.id = 'errorField'
            errorField.className = 'errorField'
            bodyContainer.appendChild(errorField)

            const patternErrorField = document.createElement("div")
            patternErrorField.id = 'patternErrorField'
            patternErrorField.className = 'patternErrorField'
            bodyContainer.appendChild(patternErrorField)
        }
    }
    return bodyContainer
}

// todo '_state' wordt gebruik bij marks, dus nog laten staan!
export const getOKFunctionOfAttributeDialog = (
    transactionOnNode: Transaction,
    state: EditorState,
    schema: MaakSchema,
    node: model.Node):any => {

    const schemaNode                        = schema.schema.nodes[node.type.name]
    const jsonSchemaNode                    = schema.jsonSchema[node.type.name]
    const attrs:any[]|undefined             = jsonSchemaNode.attrs
    const isMark:boolean                    = jsonSchemaNode.mark

    if (isMark || typeof(attrs) === 'undefined' || (typeof(attrs) !== 'undefined' && attrs.length === 0)) {
        // isMark OR no attrs, so show 'no attrs screen'
        //   then.. do nothing yet

    } else {
        // is node
        const fieldAttrs:{[key:string]:string} = {}
        for (const field of attrs) {
            const fieldId:string = Object.keys(field)[0]
            const fieldObject:any = field[fieldId]
            const formField:HTMLFormElement =
                document.querySelector('#' + fieldId.replace(":", "")) as HTMLFormElement

            // validation
            const errorField = document.getElementById('errorField') as HTMLDivElement
            const patternErrorField = document.getElementById('patternErrorField') as HTMLDivElement
            if (
                validateFieldRequired(errorField, fieldObject, formField.value) &&
                validateFieldPattern(patternErrorField, fieldObject, formField.value, fieldId)) {

                fieldAttrs[fieldId] = formField.value
            }
        }

        // pas als alle velden kloppen... opslaan
        if (attrs.length === Object.keys(fieldAttrs).length) {
            const nodeWithAttrs = (
                transactionOnNode.setNodeType(
                    transactionOnNode.selection.from,
                    (schemaNode as NodeType),
                    fieldAttrs) as Transaction)
            state.apply(nodeWithAttrs)
        }
    }
}

export const validateFieldRequired = (errorField:HTMLDivElement, fieldObject:any, formFieldValue:string):boolean => {
    if ("required" in fieldObject && fieldObject.required && formFieldValue === '') {
        addError( errorField, 'Vul alle verplichte* velden in!')
        return false
    }
    return true
}

export const validateFieldPattern = (
    patternErrorField:HTMLDivElement,
    fieldObject:any,
    formFieldValue:string,
    fieldId: string):boolean => {

    if (fieldObject.type === 'string'
    && "pattern" in fieldObject
    && fieldObject.pattern !== ''
    && !((new RegExp(fieldObject.pattern, "i")).test(formFieldValue)) ) {
        addPatternError(patternErrorField, fieldId)
        return false
    }
    return true
}

export const addError = (errorField: HTMLDivElement, msg:string):HTMLDivElement => {
    errorField.style.display = 'block'
    errorField.innerHTML = msg
    return errorField
}

export const addPatternError = (patternErrorField: HTMLDivElement, msg:string): HTMLDivElement => {
    patternErrorField.style.display = 'block'

    let prefixText = '';
    if (patternErrorField.innerHTML === '') {
        prefixText = 'De volgende velden kloppen niet: '
    }

    patternErrorField.innerHTML = prefixText + patternErrorField.innerHTML + msg + ', '
    return patternErrorField
}

export const hideErrors = (errorFields:HTMLDivElement[]):HTMLDivElement[] => {

    for (const errorField of errorFields) {
        if (errorField) {
            errorField.style.display = 'none'
            errorField.innerHTML = ''
        }
    }
    return errorFields
}

const getAttributeValueWithinSelection = (state:EditorState, attribute:string,transactionOnNode:Transaction):string => {

    const elementsInSelection:any = getNodeOrMarkFromSelection(state, transactionOnNode)
    if (elementsInSelection !== undefined
        && elementsInSelection.attrs !== undefined
        && elementsInSelection.attrs[attribute] !== undefined) {

        const valueOfAttribute = elementsInSelection.attrs[attribute]
        return valueOfAttribute;
    }
    return ''
}

export const getNodeOrMarkFromSelection = (_state:EditorState, transactionOnNode:Transaction):undefined|Node|Mark => {
    const ref = transactionOnNode.selection as NodeSelection
    if (ref.from !== ref.to) {
        const nodeSelection = new NodeSelection(ref.$from)

        // test if mark available first
        if (
            nodeSelection.node.marks !== undefined && nodeSelection.node.marks.length > 0) {
            return nodeSelection.node.marks[0]

        } else if (nodeSelection.node !== undefined) {
            return nodeSelection.node
        }
    }
    return undefined
}

/**
 * todo does not work yet, because schemaNode cannot be MarkType yet
 * @param attrs
 * @param schemaNode
 * @param state
 * @param dispatch
 */
// const saveAttrsFromMark = (
//     attrs:any[],
//     schemaNode: MarkType,
//     state:EditorState,
//     dispatch: (p: Transaction) => void):void => {
//     const fieldAttrs:any = {}
//     if (typeof(attrs) !== 'undefined') {
//         for (const field of attrs) {
//             const fieldId:string = Object.keys(field)[0].replace(":", "")
//             const fieldObject:{[key:string]:any;} = field[fieldId]
//             const formField:HTMLFormElement = document.querySelector('#' + fieldId) as HTMLFormElement
//
//             // validation
//             //  let op: info voor json: /^[a-z0-9]+$/i  en  \\w*  zijn hetzelfde! maar gebruik \\w* !
//             if ("required" in fieldObject && fieldObject.required && formField.value === '') {
//                 addError('Vul alle verplichte* velden in!');
//
//             } else if (
//                 fieldObject.type === 'string'
//                 && "pattern" in fieldObject
//                 && fieldObject.pattern !== ''
//                 && !((new RegExp(fieldObject.pattern, "i")).test(formField.value)) ) {
//                 addPatternError(fieldId);
//
//             } else {
//                 fieldAttrs[fieldId] = formField.value
//             }
//         }
//
//         // pas als alle velden kloppen... opslaan
//         if (attrs.length === Object.keys(fieldAttrs).length) {
//             dispatch(state.tr.addMark(
//                 state.selection.from,
//                 state.selection.to,
//                 schemaNode.create(fieldAttrs)
//             ) as Transaction)
//         }
//     }
// }

/**
 * disabled function (mark does not work yet)
 * @param state
 * @param type
 * @returns {any}
 */
// const markActive = (state:EditorState, type:MarkType) => {
//     const ref = state.selection as NodeSelection;
//     const from = ref.from;
//     const $from = ref.$from;
//     const to = ref.to;
//     const empty = ref.empty;
//     if (empty) {
//         return type.isInSet(state.storedMarks || $from.marks())
//     } else {
//         return state.doc.rangeHasMark(from, to, type)
//     }
// }
