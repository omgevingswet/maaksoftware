import {EditorState, TextSelection, Transaction, NodeSelection} from 'prosemirror-state'
import {NodeView, EditorView} from 'prosemirror-view'
import {history, undo, redo } from 'prosemirror-history'
import * as model from 'prosemirror-model'
import {NodeType, ResolvedPos} from 'prosemirror-model'
import {keymap} from 'prosemirror-keymap'
import {joinBackward, joinForward, splitBlock} from 'prosemirror-commands'
import {MaakSchema} from './MaakSchema'
import {createMenu} from './EditorMenu'
import {hierarchyMenu} from './HierarchyMenu'
import {geoDialog} from './Geo'

const addItem = (pos: ResolvedPos, state: EditorState, dispatch?: (tr: Transaction) => void): void => {
    const node = pos.node().type.create()
    const tr = state.tr.insert(pos.after(), node) as Transaction
    tr.setSelection(new TextSelection(tr.doc.resolve(pos.after() + 1)))
    if (dispatch) {
        dispatch(tr)
    }
}

const handleEnter = (state: EditorState, dispatch?: (tr: Transaction) => void): boolean => {
    const {from, to, $to} = state.selection
    if (from !== to) {
        addItem($to, state, dispatch)
        return true
    } else {
        return splitBlock(state, dispatch)
    }
}

const deleteSelection = (state: EditorState, dispatch?: (tr: Transaction) => void): boolean => {
    const selection = state.selection
    if (selection instanceof NodeSelection && dispatch) {
        dispatch(state.tr.deleteSelection())
        return true
    }
    return false
}

const handleDelete = (state: EditorState, dispatch?: (tr: Transaction) => void): boolean => {
    return deleteSelection(state, dispatch) || joinForward(state, dispatch)
}

const handleBackspace = (state:EditorState, dispatch?: (tr: Transaction) => void): boolean => {
    return deleteSelection(state, dispatch) || joinBackward(state, dispatch)
}

type CreateNodeView = (node: model.Node, view: EditorView, getPos: () => number) => NodeView

const createNodeViews = (_schema: MaakSchema, _nodes: { [key: string]: NodeType }) => {
    const views: { [key: string]: CreateNodeView } = {}
/*
    Object.keys(nodes).map((name) => {
        const nodeType = nodes[name]
        if (name !== "doc" && nodeType.isBlock) { //  && !nodeType.isTextblock) {
            views[name] = menuNodeView.bind(null, schema, nodeType)
        }
    })
*/
    return views
}

export class Editor {

  div: HTMLDivElement
  schema: MaakSchema
  view: EditorView
  baseURI: string
  saveURI: string
  documentURI: string

  constructor(div: HTMLDivElement, schema: MaakSchema, xml?: Document, docURI?: string) {
    this.div = div;
    this.schema = schema;
    this.documentURI = docURI ? docURI.replace('documents', 'document') : '';
    this.saveURI = this.documentURI;
    this.baseURI = '/documents/';

    const doc = xml ? schema.fromDOM(xml.documentElement) : undefined
    const menu = createMenu(schema, this)

    const state = EditorState.create({
        doc,
        schema: schema.schema,
        plugins: [menu, history(), keymap({
          "Mod-z": undo,
          "Mod-y": redo,
          "Delete": handleDelete,
          "Backspace": handleBackspace,
          "Enter": handleEnter
        }), hierarchyMenu(schema)]
    })

    this.view = new EditorView(div, {
      state,
      handleClickOn: (view: EditorView, _pos: number, node: model.Node,
            nodePos: number, _event: MouseEvent, direct: boolean) => {
        if (node.type.name.indexOf("geometrie_") === 0) {
          if (node.content.firstChild && node.content.firstChild.text) {
            geoDialog(node.content.firstChild.text)
          }
          return true
        }
        if (node.inlineContent || !direct) {
          return false
        }
        const sel = NodeSelection.create(view.state.doc, nodePos)
        view.dispatch(view.state.tr.setSelection(sel))
        return true
      },
      nodeViews: createNodeViews(schema, schema.schema.nodes)
    })
  }

  toDOM(): Document {
    return this.schema.toDOM(this.view.state.doc, this.div.ownerDocument)
  }
}
