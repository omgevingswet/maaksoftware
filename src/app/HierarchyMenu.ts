import {EditorState, Selection, NodeSelection, TextSelection, Plugin, Transaction } from 'prosemirror-state';
import {EditorView} from 'prosemirror-view'
import * as model from 'prosemirror-model'
import {NodeType} from 'prosemirror-model'
import {MenuItem, menuBar, MenuElement, DropdownSubmenu, Dropdown} from 'prosemirror-menu'
import {MaakSchema} from './MaakSchema'
import {openAttributesDialog} from './prompt'

const getNode = (selection: Selection, depth: number): model.Node | null | undefined => {
    const $from = selection.$from
    if ($from.depth + 1 === depth) {
        return $from.nodeAfter
    } else {
        return $from.node(depth)
    }
}

export const transactionOnNode = (depth: number, state: EditorState): Transaction => {
    const {$from} = state.selection
    const sel = NodeSelection.create(state.doc,
             $from.before(depth), $from.after(depth))
    const tr = state.tr
    tr.setSelection(sel)
    return tr
}

const transactionBeforeNode = (depth: number, state: EditorState): Transaction => {
    const pos = state.selection.$from.before(depth)
    const sel = TextSelection.create(state.doc, pos)
    const tr = state.tr
    tr.setSelection(sel)
    return tr
}

const transactionAfterNode = (depth: number, state: EditorState): Transaction => {
    const pos = state.selection.$from.after(depth)
    const sel = TextSelection.create(state.doc, pos)
    const tr = state.tr
    tr.setSelection(sel)
    return tr
}

const transactionAtEndOfNode = (depth: number, state: EditorState): Transaction => {
    const selection = state.selection
    const {$from} = selection
    let pos
    if ($from.depth + 1 === depth && selection instanceof NodeSelection) {
        pos = selection.$to.pos - 1
    } else {
        pos = selection.$from.end(depth)
    }
    const sel = TextSelection.create(state.doc, pos)
    const tr = state.tr
    tr.setSelection(sel)
    return tr
}

const canChangeTo = (selection: Selection, depth: number, nodeType: NodeType): boolean => {
    const node = getNode(selection, depth)
    if (node && node.type !== nodeType) {
        const {$from, $to} = selection
        const d = depth - 1
        const index = $from.index(d)
        const indexAfter = $to.indexAfter(d)
        return $from.node(d).canReplaceWith(index, indexAfter, nodeType)
    }
    return false
}

const canInsertBefore = (selection: Selection, depth: number, nodeType: NodeType): boolean => {
    const $from = selection.$from
    if ($from.depth >= depth) {
        const index = $from.index(depth - 1)
        const parent = $from.node(depth - 1)
        return parent.canReplaceWith(index, index, nodeType)
    }
    return false
}

const canInsertAfter = (selection: Selection, depth: number, nodeType: NodeType): boolean => {
    const $from = selection.$from
    if ($from.depth >= depth) {
        const indexAfter = $from.indexAfter(depth - 1)
        const parent = $from.node(depth - 1)
        return parent.canReplaceWith(indexAfter, indexAfter, nodeType)
    }
    return false
}

const canAppend = (selection: Selection, depth: number, nodeType: NodeType): boolean => {
    const node = getNode(selection, depth)
    if (node) {
        const indexAfter = node.childCount
        return node.canReplaceWith(indexAfter, indexAfter, nodeType)
    }
    return false
}

const changeTo = (depth: number, nodeType: NodeType, state: EditorState, dispatch: (p: Transaction) => void) => {
    if (canChangeTo(state.selection, depth, nodeType)) {
        let node;
        const contentNode = getNode(state.selection, depth)
        if (contentNode && nodeType.validContent(contentNode.content)) {
            node = nodeType.createAndFill({}, contentNode.content)
        }
        if (!node) {
            node = nodeType.createAndFill()
        }
        if (node) {
            const tr = transactionOnNode(depth, state)
            dispatch(tr.replaceSelectionWith(node))
        }
    }
}

const insertBefore = (depth: number, nodeType: NodeType, state: EditorState, dispatch: (p: Transaction) => void) => {
    if (canInsertBefore(state.selection, depth, nodeType)) {
        const node = nodeType.createAndFill()
        if (node) {
            const tr = transactionBeforeNode(depth, state)
            dispatch(tr.replaceSelectionWith(node))
        }
    }
}

const insertAfter = (depth: number, nodeType: NodeType, state: EditorState, dispatch: (p: Transaction) => void) => {
    if (canInsertAfter(state.selection, depth, nodeType)) {
        const node = nodeType.createAndFill()
        if (node) {
            const tr = transactionAfterNode(depth, state)
            dispatch(tr.replaceSelectionWith(node))
        }
    }
}

const append = (depth: number, nodeType: NodeType, state: EditorState, dispatch: (p: Transaction) => void) => {
    if (canAppend(state.selection, depth, nodeType)) {
        const node = nodeType.createAndFill()
        if (node) {
            const tr = transactionAtEndOfNode(depth, state)
            dispatch(tr.replaceSelectionWith(node))
        }
    }
}

type Create = (depth: number, nodeType: NodeType, name: string) => MenuItem

const changeToMenuItem = (depth: number, nodeType: NodeType, name: string): MenuItem => {
    return new MenuItem({
        css: "",
        class: "",
        execEvent: "mousedown",
        title: name,
        label: name,
        run: changeTo.bind(null, depth, nodeType),
        select(state) { return canChangeTo(state.selection, depth, nodeType) }
    })
}

const insertBeforeMenuItem = (depth: number, nodeType: NodeType, name: string): MenuItem => {
    return new MenuItem({
        css: "",
        class: "",
        execEvent: "mousedown",
        title: name,
        label: name,
        run: insertBefore.bind(null, depth, nodeType),
        select(state) { return canInsertBefore(state.selection, depth, nodeType) }
    })
}

const insertAfterMenuItem = (depth: number, nodeType: NodeType, name: string): MenuItem => {
    return new MenuItem({
        css: "",
        class: "",
        execEvent: "mousedown",
        title: name,
        label: name,
        run: insertAfter.bind(null, depth, nodeType),
        select(state) { return canInsertAfter(state.selection, depth, nodeType) }
    })
}

const appendMenuItem = (depth: number, nodeType: NodeType, name: string): MenuItem => {
    return new MenuItem({
        css: "",
        class: "",
        execEvent: "mousedown",
        title: name,
        label: name,
        run: append.bind(null, depth, nodeType),
        select(state) { return canAppend(state.selection, depth, nodeType) }
    })
}

const createDummyEditorView = (schema: MaakSchema): EditorView => {
    const state = EditorState.create({schema: schema.schema})
    return new EditorView(document.createElement("div"), {state})
}

class HierarchyDropdown extends Dropdown {

    schema: MaakSchema

    depth: number

    label: Node

    view: EditorView

    constructor(dummyView: EditorView, schema: MaakSchema, depth: number, items: MenuElement[], options?: Object) {
        super(items, options)
        this.label = super.render(dummyView)
        this.schema = schema
        this.depth = depth
    }
    render(view: EditorView): Node {
        this.view = view
        const node = getNode(view.state.selection, this.depth)
        if (node && node.type.name !== "text") {
            const c = super.render(view)
            if (c && c.firstChild && c.firstChild instanceof Element && c.firstChild.firstChild) {
                (c.firstChild.firstChild as Text).data =
                        this.schema.jsonSchema[node.type.name].tag
            }
            return c
        }
        const emptyLabel = document.createTextNode("")
        return emptyLabel
    }
}

const makeSubmenu = (schema: MaakSchema, depth: number, create: Create, label: string): DropdownSubmenu => {
    const items: MenuElement[] = []
    const nodes = schema.schema.nodes
    Object.keys(nodes).map((nodeName: string) => {
        const n = schema.jsonSchema[nodeName]
        if (n) {
            items.push(create(depth, nodes[nodeName], n.tag))
        }
    })
    const options = {title: "", css: "", style: "", label}
    return new DropdownSubmenu(items, options)
}

const dropdown = (dummyView: EditorView, schema: MaakSchema, depth: number): Dropdown => {
    const items: MenuElement[] = []
    const create = makeSubmenu.bind(null, schema, depth)
    items.push(create(changeToMenuItem, "change to"))
    if (depth > 1) {
        items.push(create(insertBeforeMenuItem, "insert before"))
        items.push(create(insertAfterMenuItem, "insert after"))
    }
    items.push(create(appendMenuItem, "append"))
    items.push(new MenuItem({
        css: "",
        class: "",
        execEvent: "mousedown",
        title: "Delete",
        label: "Delete",
        run(state: EditorState, dispatch: (p: Transaction) => void) {
            const node = getNode(state.selection, depth)
            if (node) {
                const tr = transactionOnNode(depth, state)
                dispatch(tr.deleteSelection())
            }
        },
    }))

    items.push(new MenuItem({
        css: "",
        class: "",
        execEvent: "mousedown",
        title: "Edit Attrs",
        label: "Edit Attrs",
        run(state: EditorState, _dispatch: (p: Transaction) => void) {
            const node = getNode(state.selection, depth)

            if (node !== undefined && node !== null && node.type.name !== 'text') {
                const tr = transactionOnNode(depth, state)
                openAttributesDialog(state, tr, schema, node)
            }
        }
    }))

    const options = {title: "", css: "", style: "", label: "-"}
    return new HierarchyDropdown(dummyView, schema, depth, items, options)
}

export const hierarchyMenu = (schema: MaakSchema): Plugin => {
    const view = createDummyEditorView(schema)
    const hierarchyMenus = []
    const maxDepth = 100
    for (let depth = 1; depth < maxDepth; ++depth) {
        hierarchyMenus.push(dropdown(view, schema, depth))
    }
    const content: MenuElement[][] = [hierarchyMenus]
    const menu = menuBar({
        floating: true,
        content
    })
    return menu
}
