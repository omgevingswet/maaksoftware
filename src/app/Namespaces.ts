
export class Namespaces {
    parent: Namespaces | null
    ns: { [prefix: string]: string }
    constructor(parent: Namespaces | null = null) {
        this.parent = parent
        this.ns = {}
    }
    resolve(prefix: string): string | null {
        if (this.ns.hasOwnProperty(prefix)) {
            return this.ns[prefix]
        } else if (this.parent) {
            return this.parent.resolve(prefix)
        }
        return null
    }
    insert(prefix: string, namespace: string) {
        this.ns[prefix] = namespace
    }
    nest(): Namespaces {
        return new Namespaces(this)
    }
}
