import * as model from 'prosemirror-model'
import {Fragment, DOMSerializer, MarkSpec, NodeType, NodeSpec, Schema} from 'prosemirror-model'

interface IMaakSchemaSpecNode {
    tag: string,
    htmlTag: string,
    attrs?: any[],
    inline: boolean,
    mark: boolean,
    content: string,
    description?: string,
    any: boolean,
    root: boolean,
    namespace: string | null,
    allowedChildren?: MaakSchemaSpec
}

export type MaakSchemaSpec = { [key: string]: IMaakSchemaSpecNode }

export type NodesByTag = { [key: string]: NodeType[] }

const parseXml = (doc: Document, xml: string): DocumentFragment => {
    const frag = doc.createDocumentFragment()
    const parser = new DOMParser();
    const dom = parser.parseFromString(xml, "application/xml")
    let child = dom.documentElement.firstChild
    while (child) {
        const n = doc.importNode(child, true)
        frag.appendChild(n)
        child = child.nextSibling
    }
    return frag
}

const convertJsonToSchema = (json: MaakSchemaSpec, rootTypes: string[]): Schema => {
    const nodes: { [key: string]: NodeSpec } = {}
    const marks: { [key: string]: MarkSpec } = {}
    Object.keys(json).forEach((name: string) => {
        const node = json[name]

        // todo omdat marks en nodes nu mixed in schema, marks ook in nodes.
        //    maar marks worden sowieso via schema.schema.marks aangegeven..
        if (typeof(node.mark) !== 'undefined' && node.mark) {

            const markAttrs: { [key: string]: any } = {}
            if (typeof(node.attrs) !== 'undefined') {
                for (const attr of node.attrs) {
                    const key = Object.keys(attr)[0]
                    markAttrs[key] = {"default": attr[key].default}
                }
            }

            let markDom: model.DOMOutputSpecArray
            markDom = [node.htmlTag, {}]
        }

        const attrsx: { [key: string]: any } = {}
        if (typeof(node.attrs) !== 'undefined') {
            for (const attr of node.attrs) {
                const key = Object.keys(attr)[0]
                attrsx[key] = {"default": attr[key].default}
            }
        }
        let getContent = null
        let content: string = node.content as string
        let dom: model.DOMOutputSpecArray
        if (node.any) {
            content = "text?"
            getContent = (n: Node): Fragment => {
                let frag = Fragment.empty
                if (n instanceof Element) {
                    const xml = elementToXml(n)
                    frag = Fragment.from(schema.text(xml))
                }
                return frag
            }
            dom = [node.htmlTag, {"data-tag": node.tag, "data-type": name}]
        } else if (node.content === "") {
            dom = [node.htmlTag, {"data-tag": node.tag, "data-type": name}]
        } else {
            dom = [node.htmlTag, {"data-tag": node.tag, "data-type": name}, 0]
        }
        nodes[name] = {
            inline: node.inline || false,
            content,
            group: (node.inline === true ? 'inline' : ''),
            attrs: attrsx,
            parseDOM: [{
                tag: node.htmlTag + "[data-type='" + name + "']",
                getContent,
                getAttrs(dom: HTMLElement) {
                    const returnObject:any = {}
                    if (typeof(node.attrs) !== 'undefined') {
                        for (const attr of node.attrs) {
                            const key = Object.keys(attr)[0]
                            returnObject[key] = dom.getAttribute(key)
                        }
                    }
                    return returnObject
                }
            }],
            toDOM(n) {
                const nodeAttrs = (n.attrs === null) ? {} : n.attrs
                dom[1] = Object.assign(dom[1], nodeAttrs)  // add n.attrs to attrs of tag
                if (node.any && n.content.firstChild) {
                    const xml = n.content.firstChild.text
                    if (xml) {
                        dom[2] = parseXml(document, xml)
                    }
                }
                return dom
            },
        }
    })
    nodes.doc = {
      content: "(" + rootTypes.join("|") + ")"
    }
    nodes.text = {
      inline: true
    }

    // ff hardcoded
    marks.em = {
        parseDOM: [{tag: "i"}, {tag: "em"},
            {style: "font-style", getAttrs: (value) => value === "italic" && null}],
            toDOM() { return ["em"] }
    }

    const schema = new Schema({nodes, marks})
    return schema
}

function addAttributes(a: Element, b: Element): void {
    const attributes = a.attributes
    const l = attributes.length
    for (let i = 0; i < l; ++i) {
        const attr = attributes[i]
        if (attr.nodeValue !== null && attr.nodeName !== 'data-tag'
                && attr.nodeName !== 'data-type') {
            b.setAttribute(attr.nodeName, attr.nodeValue)
        }
    }
}

type QName = { namespace: string | null, name: string };

function getSchemaType(schema: MaakSchemaSpec, html: Element): IMaakSchemaSpecNode {
    const typeName = html.getAttribute("data-type")
    if (!typeName) {
        throw new Error("Unknown element")
    }
    const type = schema[typeName]
    if (!type) {
        if (!typeName) {
            throw new Error("Unknown element")
        }
    }
    return type
}

function getQName(type: IMaakSchemaSpecNode, html: Element): QName {
    const tag = html.getAttribute("data-tag")
    if (tag !== type.tag) {
        throw new Error(`Tags '${tag}' and '${type.tag}' do not match.`)
    }
    return {
        namespace: type.namespace || null,
        name: tag
    }
}

function convertHtmlDocument(schema: MaakSchemaSpec, html: Element): Document {
    const tag = html.getAttribute("data-tag")
    if (!tag) {
        throw Error("element has no data-tag attribute")
    }
    const impl = html.ownerDocument.implementation
    const type = getSchemaType(schema, html)
    const qname = getQName(type, html)
    const doc = impl.createDocument(qname.namespace, qname.name, null)
    convertHtmlChildren(schema, html, doc.documentElement)
    return doc
}

function convertHtmlElement(schema: MaakSchemaSpec, html: Element, xmlParent: Element): Element {
    const type = getSchemaType(schema, html)
    const qname = getQName(type, html)
    const doc = xmlParent.ownerDocument
    const xml = doc.createElementNS(qname.namespace, qname.name)
    addAttributes(html, xml)
    if (type.any) {
        // copy the content verbatim
        let c = html.firstChild;
        while (c) {
            xml.appendChild(doc.importNode(c, true))
            c = c.nextSibling
        }
    } else {
        convertHtmlChildren(schema, html, xml)
    }
    return xml
}

function convertHtmlChildren(schema: MaakSchemaSpec, html: Element, xml: Element): void {
    let child = html.firstChild
    while (child) {
        let xmlChild
        if (child.nodeType === child.ELEMENT_NODE) {
            xmlChild = convertHtmlElement(schema, child as Element, xml)
        } else {
            xmlChild = child.cloneNode(true)
        }
        if (xmlChild) {
            xml.appendChild(xmlChild)
        }
        child = child.nextSibling
    }
}

function htmlElement(xml: Element, typeName: string, childType: IMaakSchemaSpecNode, doc: Document): Element {
    const html = doc.createElement(childType.htmlTag)
    if (xml.localName) {
        html.setAttribute('data-tag', xml.localName)
        html.setAttribute('data-type', typeName)
    }
    addAttributes(xml, html)
    return html
}

function getType(xml: Element, parentType: IMaakSchemaSpecNode): {typeName: string, type: IMaakSchemaSpecNode} {
    const xmlTag = xml.localName
    if (xmlTag === null) {
        throw new Error("No localName defined for XML element")
    }
    let typeName
    let type
    if (parentType.allowedChildren) {
        const c = parentType.allowedChildren
        Object.keys(c).map((name) => {
            const node = c[name]
            if (node && node.tag === xmlTag) {
                typeName = name
                type = node
            }
        })
    }
    if (!typeName || !type) {
        throw new Error(xmlTag + " is not allowed in " + parentType.tag)
    }
    return {typeName, type}
}

function convertXmlDocument(xml: Element, typeName: string, type: IMaakSchemaSpecNode, doc: Document): Element {
    const html = htmlElement(xml, typeName, type, doc)
    convertXmlChildren(xml, type, html)
    return html
}

function convertXmlElement(xml: Element, parentType: IMaakSchemaSpecNode, doc: Document): Element {
    const {typeName, type} = getType(xml, parentType)
    const html = htmlElement(xml, typeName, type, doc)
    convertXmlChildren(xml, type, html)
    return html
}

const elementToXml = (element: Element) => {
    const s = new XMLSerializer()
    return s.serializeToString(element)
}

function convertXmlChildren(xml: Element, parentType: IMaakSchemaSpecNode, html: Element): void {
    if (parentType.any) {
        // copy the elements unchanged into the html dom
        const doc = html.ownerDocument
        let c = xml.firstChild
        while (c) {
            const n = doc.importNode(c, true)
            html.appendChild(n)
            c = c.nextSibling
        }
        return
    }
    let child = xml.firstChild
    while (child) {
        let htmlChild
        if (child.nodeType === child.ELEMENT_NODE) {
            htmlChild = convertXmlElement(child as Element, parentType, xml.ownerDocument)
        } else {
            htmlChild = child.cloneNode(true)
        }
        if (htmlChild) {
            html.appendChild(htmlChild)
        }
        child = child.nextSibling
    }
}

const createNodesByTag = (jsonSchema: MaakSchemaSpec, schema: Schema): NodesByTag => {
    const nodes = schema.nodes
    const nodesByTag: NodesByTag = {}
    const keys = Object.keys(jsonSchema)
    keys.forEach((key) => {
        nodesByTag[jsonSchema[key].tag] = []
    })
    Object.keys(nodesByTag).forEach((tag) => {
        keys.forEach((key) => {
            if (jsonSchema[key].tag === tag) {
                nodesByTag[tag].push(nodes[key])
            }
        })
    })
    return nodesByTag
}

const fillAllowedChildren = (node: IMaakSchemaSpecNode, schema: MaakSchemaSpec) => {
    const allowed: MaakSchemaSpec = {}
    node.content.split(/\W/).map((v) => {
        if (v.length && v !== 'doc' && v !== 'text') {
            allowed[v] = schema[v]
        }
    })
    node.allowedChildren = allowed
}

const fillAllAllowedChildren = (schema: MaakSchemaSpec) => {
    Object.keys(schema).map((key) => {
        fillAllowedChildren(schema[key], schema)
    })
}

export class MaakSchema {

    jsonSchema: MaakSchemaSpec

    rootTypes: string[]

    schema: Schema

    _nodesByTag: NodesByTag

    schemaName: string

    constructor(jsonSchema: MaakSchemaSpec, rootTypes: string[], schemaName:string = "") {
        this.schemaName = schemaName
        this.rootTypes = rootTypes
        fillAllAllowedChildren(jsonSchema)
        this.jsonSchema = jsonSchema
        this.schema = convertJsonToSchema(jsonSchema, rootTypes)
        rootTypes.map((rootType) => {
            if (!jsonSchema[rootType]) {
                throw new Error("jsonSchema does not have a node for " + rootType)
            }
        })
        this._nodesByTag = createNodesByTag(jsonSchema, this.schema)
    }

    nodesByTag(): NodesByTag  {
        return this._nodesByTag
    }

    fromDOM(documentElement: Element): model.Node {
        const doc = documentElement.ownerDocument
        // find the right root element
        let rootType_: string | undefined
        this.rootTypes.map((r) => {
            const type = this.jsonSchema[r]
            if (type && type.tag === documentElement.localName) {
                rootType_ = r
            }
        })
        if (!rootType_) {
            throw new Error(`${documentElement.localName} is not an allowed root element`)
        }
        const rootType: string = rootType_;
        const type = this.jsonSchema[rootType]
        const html = convertXmlDocument(documentElement, rootType, type, doc)
        return model.DOMParser.fromSchema(this.schema).parse(html)
    }

    toDOM(doc: model.Node, document: Document): Document {
        const domSerializer = DOMSerializer.fromSchema(this.schema)
        const fragment = domSerializer.serializeFragment(doc.content,
                {document})
        const documentElement = fragment.firstElementChild
        if (!documentElement) {
            throw new Error("no document element")
        }
        return convertHtmlDocument(this.jsonSchema, documentElement)
    }
}
