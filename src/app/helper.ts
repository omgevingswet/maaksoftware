export const removeLastTokenFromWord = (word:string, token:string):string => {
    if (word.indexOf(token) !== -1) {
        return (word.split(token)).slice(0, -1).join(token)
    }
    return ''
}

export const capitalizeFirstLetter = (string:string):string => {
    return string.charAt(0).toUpperCase() + string.slice(1);
}
