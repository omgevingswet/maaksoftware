import { expect } from 'chai'
import { MaakSchema, MaakSchemaSpec } from './MaakSchema'
import { JSDOM } from 'jsdom'
import { XMLSerializer } from './XMLSerializer'
import * as fs from 'fs'

const newDocument = (): { jsdom: JSDOM; document: Document } => {
  const jsdom = new JSDOM()     // dummy dom
  return {jsdom, document: jsdom.window.document}
}

const newMaakSchema = (schema: MaakSchemaSpec, root: string) => {
  return ((schema: MaakSchemaSpec, root: string) => {
    return new MaakSchema(schema, [root])
  }).bind(null, schema, root)
}

const jsonSchema: MaakSchemaSpec = {
  "root": {
    tag: "root",
    htmlTag: "div",
    inline: false,
    mark: false,
    any: false,
    root: true,
    namespace: null,
    content: "text*"
  }
}

describe('MaakSchema', () => {
  it('should not be instantiable with an empty object', () => {
    expect(newMaakSchema({}, "root")).to.throw()
  }),
  it('should have a root node defined', () => {
    // root !== rot
    expect(newMaakSchema(jsonSchema, "rot")).to.throw()
  })
  it('should have a doc and text node', () => {
    const schema = new MaakSchema(jsonSchema, ["root"])
    const nodes = schema.schema.nodes
    const nodeNames = Object.keys(nodes)
    expect(nodeNames.indexOf('doc')).to.not.equal(-1)
    expect(nodeNames.indexOf('text')).to.not.equal(-1)
    expect(nodeNames.length).to.equal(3)
  })
  it('should parse a DOM node', () => {
    const schema = new MaakSchema(jsonSchema, ["root"])
    const {document: doc} = newDocument(); // ik ga met doc nieuwe elmtn maken, en kijk dan of ik kan runnen met schema
    const root = doc.createElementNS(null, 'root')
    const text = doc.createTextNode('hello')
    root.appendChild(text)
    const node = schema.fromDOM(root)
    expect(node.type.name).to.equal("doc")
    expect(node.marks.length).to.equal(0)
    expect(node.content.childCount).to.equal(1)
    expect(node.content.child(0).type).to.equal(schema.schema.nodes.root)
    expect(node.content.child(0).childCount).to.equal(1)
    expect(node.content.child(0).child(0).text).to.equal('hello')
  })
  it('should serialize to a DOM node', () => {
    const schema = new MaakSchema(jsonSchema, ["root"])
    const {document: doc} = newDocument()   // schoonmaken, want vaker gebruikt (alle doc)
    const root = doc.createElementNS(null, 'root')
    const text = doc.createTextNode('hello')
    root.appendChild(text)
    const node = schema.fromDOM(root)
    const xml = schema.toDOM(node, doc).documentElement
    expect(xml.isEqualNode(root)).to.equal(true)
  })
  it('should be instantiatable with a large schema', () => {
    const json = fs.readFileSync("public/schemas/omgevingsplan.json", 'utf8')
    const jsonSchema = JSON.parse(json)
    const schema = new MaakSchema(jsonSchema, ["toestand_2"])
    expect(schema).to.not.equal(null);
  })
  it('should be able to parse a document for a large schema', () => {
    const json = fs.readFileSync("public/schemas/omgevingsplan.json", 'utf8')
    const jsonSchema = JSON.parse(json)
    const schema = new MaakSchema(jsonSchema, ["toestand_2"])
    const xml = `<toestand xmlns='https://www.overheid.nl/namespaces/stop'>
  <metadata><uitspraak/></metadata>
  <regeling><regeling-tekst/></regeling>
</toestand>`
    const dom = new JSDOM(xml, {contentType: "text/xml"})
    const node = schema.fromDOM(dom.window.document.documentElement)
    expect(node).to.not.equal(null);
  })
  it('should be able to parse a document for a large schema', () => {
    const json = fs.readFileSync("public/schemas/omgevingsplan.json", 'utf8')
    const jsonSchema = JSON.parse(json)
    const schema = new MaakSchema(jsonSchema, ["toestand_2"])
    const xml = `<toestand xmlns='https://www.overheid.nl/namespaces/stop'>` +
        `<metadata><uitspraak/></metadata><regeling><regeling-tekst/></regeling></toestand>`
    const dom = new JSDOM(xml, {contentType: "text/xml"})
    const node = schema.fromDOM(dom.window.document.documentElement)
    const out = schema.toDOM(node, dom.window.document).documentElement
    const s = new XMLSerializer()
    const o = s.serializeToString(out)
    // TODO: support namespaces!!
    expect(o).to.equal(xml.replace(" xmlns='https://www.overheid.nl/namespaces/stop'", ""))
  })
})
