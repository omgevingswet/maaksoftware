import { Validator } from './validator'
import { expect } from 'chai'

describe('A Validator for STOP', () => {
  const catalogDir = require("path").resolve("catalog")
  const validator = new Validator("pr04schemas/stop_2017-07-06.xsd", catalogDir)
  it('finds no problems with a valid document.', (done) => {
    const Readable = require('stream').Readable
    const s = new Readable()
    s.push(`<toestand xmlns='https://www.overheid.nl/namespaces/stop'>
  <metadata><uitspraak/></metadata>
  <regeling><regeling-tekst/></regeling>
</toestand>`)
    s.push(null)
    validator.validateStream(s, (result) => {
        if (!result.valid) {
            console.log(result.report)
        }
        expect(result.valid).to.equal(true)
        done()
    })
  });
  it('finds problems with an invalid document.', (done) => {
    const Readable = require('stream').Readable
    const s = new Readable()
    s.push("<wrong/>")
    s.push(null)
    validator.validateStream(s, (result) => {
        expect(result.valid).to.equal(false)
        done()
    })
  });
});
