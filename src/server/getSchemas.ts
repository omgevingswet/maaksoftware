import * as fs from 'fs';
import * as path from 'path';
import { MaakSchemaSpec } from '../app/MaakSchema';
import { endsWith } from './helper';

const schemaDir = path.join(__dirname, '../../public/schemas');

export function getSchemaIndex(): Promise<Object> {
    return new Promise((resolve: Function, reject: Function) => {

        fs.readdir(schemaDir, (error: Error, response = []) => {

            if (error) {
                reject(error);
                return;
            }

            const schemaFiles: string[] = response.filter((name: string): boolean => endsWith(name, '.json'));
            const schemaNames: string[] = schemaFiles.map((fileName: string) => fileName.replace(/\.json$/, ''));

            const schemas = schemaNames.reduce((result: Object[], schema: string, index: number): Object[] => {
                result.push({
                    name: schema,
                    file: `/schema/${schemaFiles[index]}`
                });

                return result;
            }, [])

            resolve(schemas);
        })
    });
};

export function getSchema(schemaName: string): Promise<{ name: string, schema: MaakSchemaSpec }> {
    return new Promise((resolve: Function, reject: Function) => {
        fs.readFile(path.join(schemaDir, `${schemaName}.json`), 'utf-8', (error: Error, data: string) => {
            if (error) {
                reject(error);
                return;
            }

            resolve({
                name: schemaName,
                json: JSON.parse(data)
            });
        })
     });
}
