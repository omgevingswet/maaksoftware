import { Router, Request, Response } from 'express';
import * as path from 'path';
import * as fs from 'fs';
// import { handleError } from '../helper';

const router = Router();

type URIs = {
    schemaURI: string | null,
    documentURI: string | null
}

const getURIs = (schemaName: string, fileName?: string): URIs => {
    const result: URIs = {
        schemaURI: null,
        documentURI: null
    };

    const publicDir = path.resolve(__dirname, '../../../public');
    const schemaPath = path.resolve(publicDir, 'schemas', `${schemaName}.json`);

    // Check if filePath exists when fileName is provided
    if (fileName) {
        const filePath = path.resolve(publicDir, 'documents', schemaName, fileName);

        if (!fs.existsSync(filePath)) {
            return result;
        }

        // Use server path for document if document exists
        result.documentURI = filePath.replace(/.+\/public\//, '/');
    }

    // Check if schemaPath exists and is readable
    try {
        JSON.parse(fs.readFileSync(schemaPath, 'utf-8') as string);
    } catch (e) {
        return result;
    }

    // Use server path for schema if schema exists
    result.schemaURI = schemaPath.replace(/.+\/public\//, '/');

    return result;
}

router.get('/:schema', function(req: Request, res: Response) {
    const { schemaURI } = getURIs(req.params.schema);

    if (!schemaURI) {
        res.status(404).send(`Schema not found: ${req.params.schema}`);
    }

    res.render('editor', { schemaURI });
})

router.get('/:schema/:file', function(req: Request, res: Response) {
    const { schemaURI, documentURI } = getURIs(req.params.schema, req.params.file);

    if (!schemaURI) {
        res.status(404).send(`Schema not found: ${req.params.schema}`);
    } else if (!documentURI) {
        res.status(404).send(`Document not found: ${req.params.file}`);
    }

    res.render('editor', { schemaURI, documentURI });
});

module.exports = router;
