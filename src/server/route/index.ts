import {Router, Request, Response} from 'express';
import { getSchemaIndex } from '../getSchemas';
import * as getDocuments from '../getDocuments';
import * as _ from 'lodash';

const router = Router();

router.get('/', function(_req: Request, res: Response) {
    getSchemaIndex()
    .then((schemas) => {
        return _.map(schemas as ArrayLike<Object>, (schema: {name: string, file: string }) => {
            const documents = getDocuments.index(schema.name);
            return { schema: schema.name, documents };
        });
    })
    .then((result) => {
        console.log(result);
        res.render('index', {result});
    })
});

module.exports = router;
