import { Router, Request, Response } from 'express';
import * as path from 'path';
import * as fs from 'fs';
// import { handleError } from '../helper';

const copySync = (from: string, to: string) => {
    fs.writeFileSync(to, fs.readFileSync(from));
};

const createMetadata = (map: any): string => {
    return [
        '<?xml version="1.0" encoding="utf-8"?>',
        '<rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:dcterms="http://purl.org/dc/terms/">',
        ...Object.keys(map).map((tagName: string) => {
            return `  <${tagName}>${map[tagName]}</${tagName}>`;
        }),
        '</rdf:RDF>'
    ].join('\n');
};

const router = Router();

router.get('/:schema/:file', function(req: Request, res: Response) {
    const publicDir = path.resolve(__dirname, '../../../public');
    const schemaName = req.params.schema;
    const fileName = req.params.file;

    // Get schema.json
    const schemaPath = path.resolve(publicDir, 'schemas', `${schemaName}.json`);
    const filePath = path.resolve(publicDir, 'documents', schemaName, fileName);

    let schema;

    try {
        schema = require(schemaPath);
    } catch (e) {
        res.status(404).send('Schema not found!');
        return;
    }

    if (!fs.existsSync(filePath)) {
        res.status(404).send('Document not found!');
        return;
    }

    res.status(200);
    res.write(fs.readFileSync(filePath));
    res.end();
});

router.put('/:schema/:file', function(req: Request, res: Response) {
    const publicDir = path.resolve(__dirname, '../../../public');
    const schemaName = req.params.schema;
    const fileName = req.params.file;

     // Get schema.json
    const schemaPath = path.resolve(publicDir, 'schemas', `${schemaName}.json`);
    const filePath = path.resolve(publicDir, 'documents', schemaName, fileName);

    let schema;

    try {
        schema = require(schemaPath);
    } catch (e) {
        res.status(404).send('Schema not found!');
        return;
    }

    if (!fs.existsSync(filePath)) {
        fs.writeFile(filePath, '', (err) => {
            res.status(404).send(err);
            console.log('saved file', filePath)
        })
    }

    const tmpPath = `${filePath}.tmp`;
    console.log(`Write to temp file: ${tmpPath}`);
    const fileStream = fs.createWriteStream(tmpPath);
    req.pipe(fileStream);
    fileStream.on('close', () => {

        const timeStamp = new Date().toISOString();

        const backupPath = `${filePath}.${timeStamp}.old`;
        fs.renameSync(filePath, backupPath);
        fs.renameSync(tmpPath, filePath);

        const documentName = path.basename(filePath, '.xml');
        const documentDir = path.resolve(path.dirname(tmpPath), documentName);
        const versionsDir = path.resolve(documentDir, 'snapshots');

        if (!fs.existsSync(documentDir)) {
            fs.mkdirSync(documentDir);
        }

        if (!fs.existsSync(versionsDir)) {
            fs.mkdirSync(versionsDir);

            const firstVersionDir = path.resolve(versionsDir, 'v1');
            fs.mkdirSync(firstVersionDir);

            // Start with the old document as version 1
            copySync(backupPath, path.resolve(firstVersionDir, 'index.xml'));

            const stat = fs.statSync(backupPath);
            const metadata = createMetadata({
                'dcterms:created': stat.birthtime.toISOString()
            });

            fs.writeFileSync(path.resolve(firstVersionDir, 'metadata.xml'), metadata);
        }

        const numberSort = (a: number, b: number) => a > b ? 1 : a === b ? 0 : -1;

        const versions = fs.readdirSync(versionsDir)
            .filter((fileName) => /^v(\d+)$/.test(fileName))
            .map((fileName) => fileName.replace(/^v(\d+)$/, '$1'))
            .map(Number)
            .sort(numberSort);

        console.log('versions:', versions);

        const lastVersion = versions[versions.length - 1] || 0;
        const nextVersion = lastVersion + 1;

        console.log('lastVersion: ', lastVersion);
        const newVersionDir = path.resolve(versionsDir, `v${nextVersion}`);

        fs.mkdirSync(newVersionDir);

        copySync(filePath, path.resolve(newVersionDir, 'index.xml'));

        const metadata = createMetadata({
            'dcterms:modified': timeStamp
        });

        fs.writeFileSync(path.resolve(newVersionDir, 'metadata.xml'), metadata);

        console.log(versionsDir);
        console.log(newVersionDir);

        copySync(
            path.resolve(newVersionDir, 'index.xml'),
            path.resolve(documentDir, 'index.xml')
        );
        copySync(
            path.resolve(newVersionDir, 'metadata.xml'),
            path.resolve(documentDir, 'metadata.xml')
        );

        fs.unlinkSync(backupPath);

        res.status(200);
        res.end();
    });
});

module.exports = router;
