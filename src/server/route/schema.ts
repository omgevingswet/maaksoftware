import {Router, Request, Response} from 'express';
import * as fs from 'fs';
import * as path from 'path';
import { getSchemaIndex, getSchema } from '../getSchemas';

// Use helperfunctions from separate helper-fn file
import {endsWith, handleError} from '../helper';

const router = Router();

// TODO: Render page with overview of all schema's
router.get('/', (_req: Request, res: Response) => {
    res.redirect('./index.json');
});

// Send JSON array of al existing (TPOD)schemas
router.get('/index.json', (_req: Request, res: Response) => {
    getSchemaIndex()
        .then((schemas) => res.json(schemas), handleError);
});

// TODO: Send JSON schema of requested (TPOD)schema
router.get('/:schema',  (req: Request, res: Response) => {
    getSchema(req.params.schema)
        .then((schemaInfo) => {
            res.send(schemaInfo);
        }, handleError);
});

// Send a JSON array with all filenames using the :schema
// The documents are made statically available in /documents/:filename
router.get('/:schema/index.json', (req: Request, res: Response) => {
    const docDir = path.join(__dirname, '../../../public/documents', req.params.schema);

    fs.readdir(docDir, (error: Error, response = []) => {

        if (error) {
            console.warn(`Directory ${docDir} not available`);
        }

        const filenames: string[] = response.filter((name: string): boolean => {
            return endsWith(name, '.xml')
                || endsWith(name, '.rdf');
        });

        res.json(filenames);
    })
})

module.exports = router;
