import * as fs from 'fs';
import * as path from 'path';
// Use helperfunctions from separate helper-fn file
import { endsWith } from './helper';

export function index(schema: string): string[] {

    const docDir = path.join(__dirname, '../../public/documents', schema);
    const documents = fs.existsSync(docDir)
        ? fs.readdirSync(docDir)
        : [];

    const filenames: string[] = documents.filter((name: string): boolean => {
        return endsWith(name, '.xml')
            || endsWith(name, '.rdf');
    });

    return filenames;
}
