export const endsWith = (str: string, end: string): boolean => {
  return str.substr(str.length - end.length) === end
};

export const handleError = (error: Error | undefined, callback?: Function) => {
    if (error && callback) {
        callback(error);
    } else if (error) {
        throw error;
    }
};
