import * as express from 'express';
import * as exphbs from 'express-handlebars';

import * as path from 'path';
import * as fs from 'fs';
import * as webpack from 'webpack';
import * as webpackMiddleware from 'webpack-dev-middleware';
import * as webpackConfig from '../../webpack.config.js';
import * as logger from 'morgan';

import * as indexRoute from './route/index';
import * as schemaRoute from './route/schema';
import * as documentRoute from './route/document';
import * as editRoute from './route/editor';

const app = express();
const defaultPort = '1234';
const port = parseInt(process.env.PORT || defaultPort, 10);
const env = process.env.NODE_ENV || 'production';

// rebuild bundle.js when files change in src/app
if (env === 'development') {
    const configuration: webpack.Configuration = {
        entry: webpackConfig.entry,
        resolve: webpackConfig.resolve,
        output: webpackConfig.output,
        devtool: 'eval-source-map',
        module: webpackConfig.module,
        plugins: webpackConfig.plugins
    };
    const webpackMiddlewareInst = webpackMiddleware(webpack([configuration]), {publicPath: ''});

    app.use(logger('dev'));
    app.use(webpackMiddlewareInst);
    webpackMiddlewareInst.waitUntilValid(() => console.log(`available on http://localhost:${port}/`));
}

// view engine setup
app.engine('.hbs', exphbs({
  layoutsDir: path.resolve(__dirname, '../views/layouts'),
  defaultLayout: 'main',
  extname: '.hbs'
}));
app.set('view engine', '.hbs');
app.set('views', path.resolve(__dirname, '../views/')); // Set views directory for express-handlebars

// Make all files in public directory statically available
app.use(express.static(path.join(__dirname, '../../public')));
app.use(express.static(path.join(__dirname, '../../node_modules/font-awesome')))

app.put('/documents/*/*', (req, res) => {
  const templatePath = 'public' + req.originalUrl
  const pathToSave = 'public/documents/' + req.params[0] + '/'
    // check if pathToSave exists..
    fs.stat(pathToSave, function(err,_stats) {
      if (err) {
        // create dir
        fs.mkdirSync(path.join(__dirname, '../../public/documents/' + req.params[0] + '/'))
      }
    })

  const out = fs.createWriteStream(templatePath)
  req.pipe(out)
  out.on('close', () => {
    res.status(200)
    res.end("{}")
  })
})

// Use different routes specified in ./routes/
app.use('/', indexRoute as express.RequestHandler);
app.use('/schema/', schemaRoute as express.RequestHandler);
app.use('/document/', documentRoute as express.RequestHandler);
app.use('/editor/', editRoute as express.RequestHandler);

app.listen({ port });
