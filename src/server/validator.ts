import * as path from 'path'
import * as fs from 'fs'
import * as child_process from 'child_process'

/**
 * Write a catalog.xml with absolute URLs.
 * Not all software that uses SGML_CATALOG_FILES can use relative URLs.
 */
const writeCatalog = (catalogDir: string): string => {
    const w3 = path.join(catalogDir, 'w3')
    const catalog = `<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE catalog PUBLIC "-//OASIS//DTD XML Catalogs V1.1//EN"
    "http://www.oasis-open.org/committees/entity/release/1.1/catalog.dtd">
<catalog xmlns="urn:oasis:names:tc:entity:xmlns:xml:catalog">
  <rewriteURI uriStartString="http://www.w3.org/"
                 rewritePrefix="file:${w3}/"/>
  <rewriteURI uriStartString="https://www.w3.org/"
                 rewritePrefix="file://${w3}/"/>
</catalog>
`
    const catalogFile = path.join(catalogDir, 'catalog.xml')
    fs.writeFileSync(catalogFile, catalog)
    return catalogFile
}

export class Validator {

    schemaPath: string

    catalogFile: string

    constructor(schemaPath: string, catalogDir: string) {
        this.schemaPath = schemaPath
        this.catalogFile = writeCatalog(catalogDir);
    }

    validateSync(docPath: string): { valid: boolean; report: string; } {
        const env = {
            SGML_CATALOG_FILES: this.catalogFile,
            PATH: process.env.PATH
        }
        const r = child_process.spawnSync("xmllint", ["--noout", "--catalogs",
            "--nonet", "--schema", this.schemaPath, docPath], {
                timeout: 1000, // milliseconds
                env
            });
        return {
            valid: r.status === 0,
            report: r.stderr.toString()
        }
    }

    /**
     * Validate an XML stream against the XML Schema in this validator.
     */
    validateStream(input: NodeJS.ReadableStream, callback: (result: { valid: boolean; report: string; }) => void) {
        const env = {
            SGML_CATALOG_FILES: this.catalogFile,
            PATH: process.env.PATH
        }
        const handle = child_process.spawn("xmllint", ["--noout", "--catalogs",
            "--nonet", "--schema", this.schemaPath, "-"], {
                env,
                stdio: ['pipe', 'ignore', 'pipe']
            });
        input.pipe(handle.stdin);
        const chunks: Buffer[] = [];
        handle.stderr.on('data', (chunk) => {
            chunks.push(chunk as Buffer);
        });
        handle.on('exit', (code) => {
            const report = Buffer.concat(chunks).toString()
            callback({
                valid: code === 0,
                report
            })
        });
    }

}
