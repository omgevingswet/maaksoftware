const nightwatchCucumber = require('nightwatch-cucumber')

// Handles the runner, location of feature files and step definitions,
// and closing of nightwatch
const nightwatchCucumberConf = {
    //Todo: use Date.now()
	cucumberArgs: [
        '--require', './test/e2e',
        '--format', 'pretty',
        '--format', `json:tests_output/e2e/e2e.json`,
        './requirements'
    ],
    runner: 'nightwatch',
    closeSession: 'afterFeature'
}

nightwatchCucumber(nightwatchCucumberConf);

module.exports = {
    // Loads nightwatch-cucumber configuration into main nightwatch.js conf
    src_folders: ['./test/e2e'],
    custom_commands_path: '',
    custom_assertions_path: '',
    page_objects_path: '',
    live_output: false,
    disable_colors: false,


    // Sets configuration for Selenium Server
    selenium: {
        start_process: true,
        server_path: 'test/bin/selenium.jar',
        host: '127.0.0.1',
        port: 4444,
        cli_args : {
          'webdriver.chrome.driver' : 'test/bin/chromedriver',
          'webdriver.gecko.driver' : 'node_modules/geckodriver/geckodriver'
        }
    },


    // Sets config options for different testing environments defined by the user
    test_settings: {
        default: {
            launch_url: 'http://localhost:1234',
            silent: true,
            screenshots: {
                enabled: false,
                on_error: false,
                on_failure: false,
                path: 'report/screenshots'
            }
        },
        chrome: {
            desiredCapabilities: {
                browserName: 'chrome',
                javascriptEnabled: true,
                acceptSslCerts: true
            }
        },
        firefox: {
            desiredCapabilities: {
                browserName: 'firefox',
                acceptSslCerts: true
            }
        }
    }
};
