<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:fn="http://www.w3.org/2005/xpath-functions" xmlns:sch="http://purl.oclc.org/dsdl/schematron" xmlns:doc="http://technische-documentatie.oep.overheid.nl/namespaces/doc" xmlns:stte="https://test.example.com/template">
    <xsl:output method="xml" version="1.0" indent="yes" encoding="utf-8"/>

    <!-- algemeen -->

    <xsl:param name="simpleTypes" select="('xs:boolean','xs:date','xs:dateTime','xs:gDay','xs:gMonth','xs:gMonthDay','xs:gYear','xs:gYearMonth','xs:byte','xs:decimal','xs:double','xs:float','xs:hexBinary','xs:int','xs:integer','xs:long','xs:negativeInteger','xs:nonNegativeInteger','xs:nonPositiveInteger','xs:positiveInteger','xs:short','xs:unsignedByte','xs:unsignedInt','xs:unsignedLong','xs:unsignedShort','xs:anySimpleType','xs:anyURI','xs:base64Binary','xs:duration','xs:ENTITIES','xs:ENTITY','xs:ID','xs:IDREF','xs:IDREFS','xs:language','xs:Name','xs:NCName','xs:NMTOKEN','xs:NMTOKENS','xs:normalizedString','xs:NOTATION','xs:QName','xs:string','xs:time','xs:token')"/>
    <xsl:param name="simpleBaseTypes" select="('boolean','date','date','date','date','date','date','date','number','number','number','number','number','number','number','number','number','number','number','number','number','number','number','number','number','string','string','string','string','string','string','string','string','string','string','string','string','string','string','string','string','string','string','string','string')"/>

    <xsl:param name="default" select="string('string')"/>

    <xsl:template match="/">
        <implementatie>
            <xsl:for-each select="/xs:schema/xs:element/@name">
                <root name="{.}"/>
            </xsl:for-each>
            <xsl:apply-templates select="fn:document('tpod.xml',.)//klasse[@naam='stod:regeling-tekst']/implementatie/template" mode="template"/>
            <xsl:apply-templates select="xs:schema"/>
        </implementatie>
    </xsl:template>

    <!-- door te geven elementen uit tpod.xml -->

    <xsl:template match="*" mode="template">
        <xsl:element name="{name()}">
            <xsl:apply-templates select="@*|node()" mode="template"/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="@*" mode="template">
        <xsl:copy-of select="."/>
    </xsl:template>

    <xsl:template match="text()" mode="template">
        <xsl:if test="normalize-space() ne ''">
            <xsl:value-of select="."/>
        </xsl:if>
    </xsl:template>

    <xsl:template match="comment()|processing-instruction()" mode="template">
        <!-- wis alle comments en processing-instructions -->
    </xsl:template>

    <!-- door te geven elementen uit imop.xsd-->

    <xsl:template match="*/@*" priority="-1">
        <!-- wis elementen die niet expliciet doorgegeven worden -->
    </xsl:template>

    <xsl:template match="xs:annotation">
        <xsl:choose>
            <xsl:when test="parent::xs:attribute|parent::xs:element">
                <xsl:apply-templates select="xs:appinfo/doc:readableName"/>
                <xsl:apply-templates select="xs:documentation"/>
            </xsl:when>
            <xsl:otherwise>
                <!-- geef annotation niet door -->
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="doc:readableName">
        <xsl:element name="tag">
            <xsl:value-of select="fn:tokenize(.,':')[last()]"/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="xs:documentation">
        <xsl:element name="documentation">
            <xsl:apply-templates select="p|text()"/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="p">
        <xsl:value-of select="normalize-space(.)"/>
        <xsl:value-of select="if (following-sibling::p) then string(' ') else string('')"/>
    </xsl:template>

    <xsl:template match="text()">
        <xsl:if test="normalize-space() ne ''">
            <xsl:value-of select="normalize-space(.)"/>
        </xsl:if>
    </xsl:template>

    <!-- start de verwerking van elementen -->

    <xsl:template match="xs:schema">
        <xsl:element name="schema">
            <xsl:attribute name="uri" select="fn:base-uri()"/>
            <xsl:apply-templates select="." mode="start"/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="xs:schema" mode="start">
        <xsl:apply-templates select="xs:import | xs:include"/>
        <xsl:apply-templates select=".//xs:element[@name]" mode="start">
            <xsl:sort select="@name"/>
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="xs:import | xs:include">
        <xsl:apply-templates select="document(@schemaLocation)/xs:schema" mode="start"/>
    </xsl:template>

    <xsl:template match="xs:element[@name]" mode="start">
        <xsl:choose>
            <xsl:when test="@abstract='true'">
                <!-- element is niet beschikbaar -->
            </xsl:when>
            <xsl:otherwise>
                <xsl:variable name="type" select="fn:tokenize(@type,':')[last()]"/>
                <xsl:variable name="namespace-uri" select="ancestor::xs:schema/@targetNamespace"/>
                <xsl:element name="element">
                    <xsl:attribute name="name" select="@name"/>
                    <xsl:if test="$namespace-uri">
                      <xsl:attribute name="namespace-uri" select="$namespace-uri"/>
                    </xsl:if>
                    <xsl:attribute name="id" select="count(.|preceding::xs:element[@name]|ancestor::xs:element[@name])"/>
                    <xsl:apply-templates select="@doc:klasse"/>
                    <xsl:apply-templates select="xs:simpleType|ancestor::xs:schema/xs:simpleType[@name=$type]"/>
                    <xsl:apply-templates select="xs:complexType|ancestor::xs:schema/xs:complexType[@name=$type]"/>
                    <xsl:apply-templates select="xs:annotation"/>
                </xsl:element>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="xs:element">
        <xsl:param name="minOccurs">
            <xsl:sequence select="node()"/>
        </xsl:param>
        <xsl:param name="maxOccurs">
            <xsl:sequence select="node()"/>
        </xsl:param>
        <xsl:variable name="name" select="(@name,fn:tokenize(@ref,':')[last()])"/>
        <!-- bereken het id -->
        <xsl:variable name="test" select="(ancestor::xs:element[@name]|ancestor::xs:group[@name]|ancestor::xs:complexType[@name])[1]"/>
        <xsl:variable name="id" as="xs:integer">
            <xsl:choose>
                <xsl:when test="self::xs:element[@name]">
                    <xsl:value-of select="count(.|preceding::xs:element[@name]|ancestor::xs:element[@name])"/>
                </xsl:when>
                <xsl:when test="$test/name()='xs:element'">
                    <xsl:value-of select="count(ancestor::xs:schema/xs:element[@name=$name]/(.|preceding::xs:element[@name]|ancestor::xs:element[@name]))"/>
                </xsl:when>
                <xsl:when test="$test/name()='xs:group'">
                    <xsl:value-of select="count(ancestor::xs:schema/xs:element[@name=$name]/(.|preceding::xs:element[@name]|ancestor::xs:element[@name]))"/>
                </xsl:when>
                <xsl:when test="$test/name()='xs:complexType'">
                    <xsl:value-of select="count(ancestor::xs:schema/xs:element[@name=$name]/(.|preceding::xs:element[@name]|ancestor::xs:element[@name]))"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="0"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <!-- controleer of het element abstract is -->
        <xsl:choose>
            <xsl:when test="(ancestor::xs:schema//xs:element[@name])[$id]/@abstract='true'">
                <!-- element is niet beschikbaar -->
            </xsl:when>
            <xsl:otherwise>
                <xsl:element name="element">
                    <xsl:attribute name="name" select="$name"/>
                    <xsl:attribute name="id" select="$id"/>
                    <xsl:apply-templates select="$minOccurs|$maxOccurs"/>
                    <xsl:apply-templates select="@minOccurs|@maxOccurs"/>
                </xsl:element>
            </xsl:otherwise>
        </xsl:choose>
        <!-- substitueer mogelijke substitutiongroups -->
        <xsl:variable name="substitutionGroup" select="ancestor::xs:schema//xs:element[@name][fn:tokenize(@substitutionGroup,':')[last()]=$name]"/>
        <xsl:if test="$substitutionGroup">
            <xsl:apply-templates select="$substitutionGroup">
                <xsl:with-param name="minOccurs" select="@minOccurs"/>
                <xsl:with-param name="maxOccurs" select="@maxOccurs"/>
            </xsl:apply-templates>
        </xsl:if>
    </xsl:template>

    <!-- elementen en attributen binnen element -->

    <xsl:template match="@doc:klasse">
        <xsl:attribute name="class">
            <xsl:value-of select="."/>
        </xsl:attribute>
    </xsl:template>

    <xsl:template match="@minOccurs">
        <xsl:attribute name="minOccurs">
            <xsl:value-of select="."/>
        </xsl:attribute>
    </xsl:template>

    <xsl:template match="@maxOccurs">
        <xsl:attribute name="maxOccurs">
            <xsl:value-of select="."/>
        </xsl:attribute>
    </xsl:template>

    <xsl:template match="xs:complexType">
        <xsl:apply-templates select="@mixed"/>
        <xsl:apply-templates select="xs:complexContent|xs:simpleContent"/>
        <xsl:apply-templates select="xs:all|xs:choice|xs:group|xs:sequence"/>
        <xsl:apply-templates select="xs:anyAttribute|xs:attribute|xs:attributeGroup"/>
        <xsl:apply-templates select="xs:annotation"/>
    </xsl:template>

    <xsl:template match="xs:simpleType">
        <xsl:apply-templates select="xs:list|xs:restriction|xs:union"/>
        <xsl:apply-templates select="xs:annotation"/>
    </xsl:template>

    <!-- elementen en attributen binnen complexType -->

    <xsl:template match="@mixed[true()]">
        <xsl:attribute name="content">
            <xsl:value-of select="string('mixed')"/>
        </xsl:attribute>
    </xsl:template>

    <xsl:template match="xs:all">
        <xsl:element name="all">
            <xsl:apply-templates select="xs:element">
                <xsl:with-param name="minOccurs" select="@minOccurs"/>
                <xsl:with-param name="maxOccurs" select="@maxOccurs"/>
            </xsl:apply-templates>
        </xsl:element>
    </xsl:template>

    <xsl:template match="xs:choice">
        <xsl:variable name="id" select="(ancestor-or-self::xs:element[@name]|ancestor::xs:group[@name]|ancestor::xs:complexType[@name])[1]"/>
        <xsl:element name="choice">
            <xsl:attribute name="{$id/local-name()}" select="$id/@name"/>
            <xsl:apply-templates select="xs:any|xs:choice|xs:element|xs:group|xs:sequence">
                <xsl:with-param name="minOccurs" select="@minOccurs"/>
                <xsl:with-param name="maxOccurs" select="@maxOccurs"/>
            </xsl:apply-templates>
        </xsl:element>
    </xsl:template>

    <xsl:template match="xs:complexContent">
        <xsl:apply-templates select="@mixed"/>
        <xsl:apply-templates select="xs:extension|xs:restriction"/>
        <xsl:apply-templates select="xs:annotation"/>
    </xsl:template>

    <xsl:template match="xs:group">
        <xsl:variable name="ref" select="fn:tokenize(@ref,':')[last()]"/>
        <xsl:apply-templates select="ancestor::xs:schema/xs:group[@name=$ref]/node()">
            <xsl:with-param name="minOccurs" select="@minOccurs"/>
            <xsl:with-param name="maxOccurs" select="@maxOccurs"/>
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="xs:sequence">
        <xsl:variable name="id" select="(ancestor-or-self::xs:element[@name]|ancestor::xs:group[@name]|ancestor::xs:complexType[@name])[1]"/>
        <xsl:element name="sequence">
            <xsl:attribute name="{$id/local-name()}" select="$id/@name"/>
            <xsl:apply-templates select="xs:any|xs:choice|xs:element|xs:group|xs:sequence">
                <xsl:with-param name="minOccurs" select="@minOccurs"/>
                <xsl:with-param name="maxOccurs" select="@maxOccurs"/>
            </xsl:apply-templates>
        </xsl:element>
    </xsl:template>

    <xsl:template match="xs:simpleContent">
        <xsl:apply-templates select="xs:extension|xs:restriction"/>
        <xsl:apply-templates select="xs:annotation"/>
    </xsl:template>

    <!-- elementen en attributen binnen complexContent -->

    <xsl:template match="xs:extension">
        <xsl:variable name="base" select="if (fn:starts-with(@base,'xs:')) then @base else fn:tokenize(@base,':')[last()]"/>
        <xsl:choose>
            <xsl:when test="ancestor::xs:schema/(xs:complexType[@name=$base]|xs:simpleType[@name=$base])">
                <xsl:apply-templates select="ancestor::xs:schema/(xs:complexType[@name=$base]|xs:simpleType[@name=$base])"/>
                <xsl:apply-templates select="xs:all|xs:choice|xs:group|xs:sequence"/>
                <xsl:apply-templates select="xs:anyAttribute|xs:attribute|xs:attributeGroup"/>
                <xsl:apply-templates select="xs:annotation"/>
            </xsl:when>
            <xsl:otherwise>
                <!-- als er geen doorverwijzing is, zal het een 'echt' simpleType zijn -->
                <xsl:element name="datatype">
                    <xsl:variable name="index" select="fn:index-of($simpleTypes,string($base))"/>
                    <xsl:attribute name="value" select="if ($index gt 0) then $simpleBaseTypes[$index] else $default"/>
                    <xsl:apply-templates select="xs:all|xs:choice|xs:group|xs:sequence"/>
                    <xsl:apply-templates select="xs:anyAttribute|xs:attribute|xs:attributeGroup"/>
                    <xsl:apply-templates select="xs:annotation"/>
                </xsl:element>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="xs:restriction">
        <xsl:variable name="base" select="if (fn:starts-with(@base,'xs:')) then @base else fn:tokenize(@base,':')[last()]"/>
        <xsl:variable name="index" select="fn:index-of($simpleTypes,string($base))"/>
        <xsl:attribute name="value" select="if ($index gt 0) then $simpleBaseTypes[$index] else $default"/>
        <xsl:apply-templates select="ancestor::xs:schema/(xs:complexType[@name=$base]|xs:simpleType[@name=$base])"/>
        <xsl:for-each-group select="*" group-by="name()">
            <xsl:choose>
                <xsl:when test="current-grouping-key()='xs:enumeration'">
                    <xsl:attribute name="value" select="string('enumeration')"/>
                    <xsl:element name="enumeration">
                        <xsl:apply-templates select="current-group()"/>
                    </xsl:element>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:apply-templates select="current-group()"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:for-each-group>
    </xsl:template>

    <!-- elementen en attributen binnen choice en sequence -->

    <xsl:template match="@processContents">
        <xsl:attribute name="processContents">
            <xsl:value-of select="."/>
        </xsl:attribute>
    </xsl:template>

    <xsl:template match="xs:any">
        <xsl:param name="minOccurs">
            <xsl:sequence select="node()"/>
        </xsl:param>
        <xsl:param name="maxOccurs">
            <xsl:sequence select="node()"/>
        </xsl:param>
        <xsl:element name="any">
            <xsl:apply-templates select="$minOccurs|$maxOccurs"/>
            <xsl:apply-templates select="@minOccurs|@maxOccurs|@processContents"/>
        </xsl:element>
    </xsl:template>

    <!-- elementen binnen simpleType -->

    <xsl:template match="xs:list">
        <xsl:variable name="itemType" select="if (fn:starts-with(@itemType,'xs:')) then @itemType else fn:tokenize(@itemType,':')[last()]"/>
        <xsl:variable name="index" select="fn:index-of($simpleTypes,string($itemType))"/>
        <xsl:attribute name="value" select="if ($index gt 0) then $simpleBaseTypes[$index] else $default"/>
        <xsl:apply-templates select="ancestor::xs:schema/xs:simpleType[@name=$itemType]"/>
        <xsl:apply-templates select="xs:simpleType"/>
        <xsl:apply-templates select="xs:annotation"/>
    </xsl:template>

    <xsl:template match="xs:union">
        <xsl:variable name="memberTypes" select="fn:tokenize(@memberTypes,'\s+')"/>
        <xsl:element name="datatype">
            <xsl:for-each-group select="ancestor::xs:schema/xs:simpleType[@name=$memberTypes]/*" group-by="name()">
                <xsl:for-each select="current-grouping-key()">
                    <xsl:variable name="union">
                        <xsl:element name="{current-grouping-key()}">
                            <xsl:attribute name="base" select="current-group()[1]/@base"/>
                            <xsl:copy-of select="current-group()/*"/>
                        </xsl:element>
                    </xsl:variable>
                    <xsl:apply-templates select="$union"/>
                </xsl:for-each>
            </xsl:for-each-group>
        </xsl:element>
    </xsl:template>

    <!-- elementen binnen restriction -->

    <xsl:template match="xs:enumeration">
        <xsl:element name="item">
            <xsl:value-of select="@value"/>
            <xsl:apply-templates select="xs:annotation"/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="xs:fractionDigits|xs:length|xs:maxExclusive|xs:maxInclusive|xs:maxLength|xs:minExclusive|xs:minInclusive|xs:minLength|xs:pattern|xs:totalDigits|xs:whiteSpace">
        <xsl:element name="{local-name()}">
            <xsl:value-of select="@value"/>
            <xsl:apply-templates select="xs:annotation"/>
        </xsl:element>
    </xsl:template>

    <!-- door te geven attributen -->

   <xsl:template match="xs:anyAttribute">
       <xsl:element name="any">
           <xsl:apply-templates select="@processContents"/>
       </xsl:element>
   </xsl:template>

    <xsl:template match="xs:attribute">
        <xsl:variable name="type" select="if (fn:starts-with(@type,'xs:')) then @type else fn:tokenize(@type,':')[last()]"/>
        <xsl:element name="attribute">
            <xsl:apply-templates select="(@default|@fixed)|(@name|@ref)|@use"/>
            <xsl:element name="datatype">
                <xsl:choose>
                    <xsl:when test="ancestor::xs:schema/xs:simpleType[@name=$type]">
                        <xsl:apply-templates select="ancestor::xs:schema/xs:simpleType[@name=$type]"/>
                        <xsl:apply-templates select="xs:simpleType"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <!-- dit zal een 'echt' simpleType zijn -->
                        <xsl:variable name="index" select="fn:index-of($simpleTypes,string($type))"/>
                        <xsl:attribute name="value" select="if ($index gt 0) then $simpleBaseTypes[$index] else $default"/>
                        <xsl:apply-templates select="xs:simpleType"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:element>
            <xsl:apply-templates select="xs:annotation"/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="xs:attributeGroup">
        <xsl:variable name="ref" select="fn:tokenize(@ref,':')[last()]"/>
        <xsl:apply-templates select="ancestor::xs:schema/xs:attributeGroup[@name=$ref]"/>
        <xsl:apply-templates select="xs:anyAttribute|xs:attribute|xs:attributeGroup"/>
        <xsl:apply-templates select="xs:annotation"/>
    </xsl:template>

    <xsl:template match="@default|@fixed|@name|@ref|@use">
        <xsl:copy-of select="."/>
    </xsl:template>
</xsl:stylesheet>
