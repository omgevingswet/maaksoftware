Deze folder bevat de IMOP en IMTP bestanden die vanuit PR04 worden aangeleverd.
Deze bestanden worden omgezet naar JSON bestanden die de Maaksoftware kan inlezen.

Gebruik het volgende script om de JSON bestanden te genereren:

```
cd pr04schemas/
./createSchema.sh stop_2017-07-06.xsd
```
