<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:fn="http://www.w3.org/2005/xpath-functions">
    <xsl:output method="text" encoding="utf-8"/>
    <xsl:strip-space elements="*"/>

    <xsl:variable name="missing" select="document('missing.xml')/missing" as="element(missing)"/>

    <!-- process the document element -->
    <xsl:template match="/implementatie" as="xs:string">
        <xsl:variable name="all" as="xs:string+">
            <xsl:sequence select="'{'"/>
            <xsl:apply-templates select="schema/element"/>
            <xsl:sequence select="'}'"/>
        </xsl:variable>
        <xsl:sequence select="string-join($all,'')"/>
    </xsl:template>

    <xsl:template match="element" as="xs:string+">
        <xsl:variable name="name" select="@name" as="xs:string"/>
        <xsl:variable name="namespace-uri" select="@namespace-uri" as="xs:string?"/>
        <xsl:variable name="extra" select="$missing/element[@name=$name and @namespace-uri=$namespace-uri]" as="element(element)?"/>
        <xsl:if test="not($extra)">
          <xsl:message select="concat('No extra info for ',$name,'.')"/>
        </xsl:if>
        <xsl:variable name="class" as="xs:string" select="$extra/@class"/>
        <xsl:variable name="inline" as="xs:boolean" select="boolean($extra/@inline eq 'true')"/>
        <xsl:variable name="root" as="xs:boolean" select="count(/implementatie/root[@name=$name]) > 0"/>
        <xsl:variable name="any" as="xs:boolean" select="count($extra/@any eq 'true') > 0"/>
        <xsl:variable name="namespace-uri" as="xs:string?" select="$extra/@namespace-uri"/>
        <xsl:variable name="mixed" select="@content='mixed'" as="xs:boolean"/>
        <xsl:sequence select="concat('&quot;',fn:translate(@name,'-.','__'),'_',@id,'&quot;:{')"/>
        <xsl:sequence select="if ($namespace-uri) then concat('&quot;namespaceURI&quot;:&quot;',$namespace-uri,'&quot;,') else ''"/>
        <xsl:sequence select="concat('&quot;tag&quot;:&quot;',@name,'&quot;')"/>
        <xsl:sequence select="if (documentation) then concat(',&quot;description&quot;:&quot;',fn:replace(fn:string-join(documentation,' '),'(\\|&quot;)','\\$1'),'&quot;') else ''"/>
        <xsl:sequence select="concat(',&quot;htmlTag&quot;:&quot;',$class,'&quot;')"/>
        <xsl:sequence select="if ($inline) then ',&quot;inline&quot;:true' else ',&quot;inline&quot;:false'"/>
        <xsl:sequence select="if ($root) then ',&quot;root&quot;:true' else ',&quot;root&quot;:false'"/>
        <xsl:sequence select="if ($any) then ',&quot;any&quot;:true' else ',&quot;any&quot;:false'"/>
        <xsl:sequence select="if ($mixed) then ',&quot;mark&quot;:true' else ',&quot;mark&quot;:false'"/>
        <xsl:sequence select="',&quot;content&quot;:&quot;'"/>
        <xsl:variable name="content" as="xs:string*">
            <xsl:for-each-group select=".//element" group-adjacent="fn:string-join(ancestor::choice[parent::element|parent::sequence][1]/ancestor::*/name(),'|')">
                <xsl:variable name="group">
                    <xsl:for-each select="current-group()">
                        <xsl:element name="element">
                            <xsl:attribute name="name">
                                <xsl:sequence select="concat(fn:translate(@name,'-.','__'),'_',@id)"/>
                            </xsl:attribute>
                            <xsl:attribute name="minOccurs">
                                <xsl:sequence select="if (@minOccurs) then number(@minOccurs) else 1"/>
                            </xsl:attribute>
                            <xsl:attribute name="maxOccurs">
                                <xsl:sequence select="if (@maxOccurs) then (if (number(@maxOccurs) gt 0) then number(@maxOccurs) else 99999) else 99999"/>
                            </xsl:attribute>
                        </xsl:element>
                    </xsl:for-each>
                    <xsl:if test="$mixed">
                        <xsl:element name="element">
                            <xsl:attribute name="name" select="'text'"/>
                            <xsl:attribute name="minOccurs" select="0"/>
                        </xsl:element>
                    </xsl:if>
                </xsl:variable>
                <!-- controleer of het een choice of sequence is -->
                <xsl:choose>
                    <xsl:when test="current-group()[1]/parent::choice and (count(current-group()) gt 1)">
                        <xsl:sequence select="concat('(',fn:string-join($group/element/@name,' | '),')','{',fn:min($group/element/@minOccurs),',',translate(string(fn:max($group/element/@maxOccurs)),'99999',''),'}')"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:sequence select="fn:string-join($group/element/(concat(@name,'{',@minOccurs,',',translate(@maxOccurs,'99999',''),'}')),' ')"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:for-each-group>
        </xsl:variable>
        <xsl:sequence select="fn:string-join($content,' ')"/>
        <xsl:sequence select="'&quot;'"/>
        <!-- voeg de attributen toe -->
        <xsl:call-template name="attribute"/>
        <xsl:sequence select="'}'"/>
        <xsl:if test="following-sibling::element">
            <xsl:sequence select="','"/>
        </xsl:if>
    </xsl:template>

    <xsl:template name="attribute" as="xs:string+">
        <xsl:sequence select="',&quot;attrs&quot;:['"/>
        <xsl:for-each select="attribute">
            <xsl:sequence select="concat('{&quot;',@name,@ref,'&quot;:{')"/>
            <xsl:sequence select="concat('&quot;tag&quot;:&quot;',(tag,@name,@ref),'&quot;')"/>
            <xsl:if test="documentation">
                <xsl:sequence select="',&quot;description&quot;:&quot;'"/>
                <xsl:sequence select="fn:replace(fn:string-join(documentation,' '),'(\\|&quot;)','\\$1')"/>
                <xsl:sequence select="'&quot;'"/>
            </xsl:if>
            <xsl:sequence select="if (use='required') then concat(',&quot;required&quot;:',true()) else concat(',&quot;required&quot;:',false())"/>
            <xsl:sequence select="if (datatype) then concat(',&quot;type&quot;:&quot;',datatype/@value,'&quot;') else ''"/>
            <xsl:sequence select="if (@default) then concat(',&quot;default&quot;:&quot;',@default,'&quot;') else ',&quot;default&quot;:null'"/>
            <xsl:sequence select="if (datatype/pattern) then concat(',&quot;pattern&quot;:&quot;',replace(datatype/pattern,'\\','\\\\'),'&quot;') else ''"/>
            <xsl:sequence select="if (datatype/enumeration/item) then concat(',&quot;pattern&quot;:&quot;',fn:string-join((datatype/enumeration/item),'|'),'&quot;') else ''"/>
            <xsl:sequence select="'}}'"/>
            <xsl:if test="following-sibling::attribute">
                <xsl:sequence select="','"/>
            </xsl:if>
        </xsl:for-each>
        <xsl:sequence select="']'"/>
    </xsl:template>

    <xsl:template match="template" as="empty-sequence()">
        <!-- doe niets -->
    </xsl:template>

</xsl:stylesheet>
