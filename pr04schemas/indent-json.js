let jsonChunks = [];

process.stdin.setEncoding('utf8');

process.stdin.on('data', (chunk) => {
    if (chunk !== null) {
        jsonChunks.push(chunk);
    }
});

process.stdin.on('end', () => {
    const json = jsonChunks.join('');
    const formatted = JSON.stringify(JSON.parse(json), null, '  ');

    process.stdout.write(formatted);
    process.stdout.write('\n');
});
