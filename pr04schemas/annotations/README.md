# Voordelen van RDF annotaties voor prototype

- geen eigen syntax voor
  - minOccurs/maxOccurs
  - xs:pattern
  - xs:enumeration

- xs:element is met huidige scripts om te zetten naar ProseMirror model
- er is bestaande software (Saxon, libxml) om te valideren
- het is waarschijnlijk mogelijk profielen te maken met <xs:redefine>
- voor bepaalde constraints is het mogelijk Schematron of XML Schema 1.1 te gebruiken
- extra informatie (zoals nu in missing.xml staat) kan ook in <xs:annotation><xs:appinfo/></xs:annotation>
- RDF is makkelijk te exporteren naar diverse formaten, om later te importeren in een triple database
- de xs:simpleTypes zijn ook herbruikbaar voor <xs:attribute> zodat op dezelfde basis RDFa gebruikt kan worden

## Zelf valideren met command line

```sh
xmllint --noout --schema rdf.xsd example.rdf
```
