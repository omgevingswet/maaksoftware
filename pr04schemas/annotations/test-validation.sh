#!/usr/bin/env bash

xmllint --nonet --noout --schema rdf.xsd rdf.rdf
xmllint --nonet --noout --schema overheid.xsd overheid-Provincie.xml
