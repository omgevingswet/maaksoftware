#!/usr/bin/env bash
# exit on first error
set -o errexit

cd $(dirname $0)

# exit on first error
set -o errexit

cd $(dirname $0)

# for prefix in 'dcterms' 'overheid' 'rdf' 'stod' 'stop'; do
for prefix in 'overheid'; do
  # for prefix in 'rdf'; do

  # Temporarily copy schemas to directory of `createSchema.sh`,
  # in order for relative file references in its implementation to work.
  if [ -f $prefix-*.xsd ]; then
    cp $prefix-*.xsd ..
  fi

  # Generate JSON definition for ProseMirror based on XML Schema for metadata elements
  cd ..
  ./createSchema.sh "${prefix}.xsd"
  cd annotations/

  # Copy any example documents to the 'CMS'
  if [ -f $prefix*.xml ]; then
    mkdir -p "../../public/documents/annotatie-${prefix}/"
    cp $prefix.xml $prefix-*.xml "../../public/documents/annotatie-${prefix}/"
  fi
  if [ -f "${prefix}.rdf" ]; then
    mkdir -p "../../public/documents/annotatie-${prefix}/"
    cp $prefix.rdf "../../public/documents/annotatie-${prefix}/"
  fi

  # Copy JSON model to the 'CMS'
  cp ../schema.json "../../public/schemas/annotatie-${prefix}.json"

  # Remove temporary files
  rm -f "../$prefix.xsd"
  rm -f ../$prefix-*.xsd
done
