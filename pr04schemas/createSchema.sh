#!/usr/bin/env bash

# exit on first error
set -o errexit
# exit when undefined var is used
set -o nounset
# exit with error code of right-most command in a pipe
set -o pipefail

OUTPUT_DEFAULT='schema.json'
OUTPUT="${2:-$OUTPUT_DEFAULT}"

# Erase the previous result file.
# This way you are sure to notice when this script doesn't run correctly.
echo > "${OUTPUT}"

# go into the directory of this script
cd "$(dirname "$0")"

# check that SAXON_CP is set or look in standard location
saxon_jar=${SAXON_CP:-}
if [ -z "$saxon_jar" ]; then
    if [ -e /usr/share/java/Saxon-HE.jar ]; then
        # ubuntu and debian have libsaxonhe-java
        saxon_jar="/usr/share/java/Saxon-HE.jar"
    else
        echo "The environment variable SAXON_CP should be set and should point to"
        echo "the Saxon jar, e.g. $HOME/bin/saxon9he.jar."
        echo ""
        echo "Download Saxon HE from:"
        echo "https://downloads.sourceforge.net/project/saxon/Saxon-HE/9.8/SaxonHE9-8-0-1J.zip"
        exit 1
    fi
fi
if [ ! -f "$saxon_jar" ]; then
    echo "The file '$saxon_jar' does not exist."
    exit 1
fi

xsltargs=(-jar "$saxon_jar" -warnings:recover -dtd:off -expand:off -a:off)

java "${xsltargs[@]}" -versionmsg:off -xsl:xsd.xsl -s:"$1" -o:../tmp/tmp.xml
java "${xsltargs[@]}" -versionmsg:off -xsl:json.xsl -s:../tmp/tmp.xml -o:../tmp/schema-one-line.json
cat ../tmp/schema-one-line.json | node indent-json.js > $OUTPUT

# patch out problematic parts
perl -pi -e 's/ text\{0,}//' "${OUTPUT}"
perl -pi -e 's/nr_98\{0,} nr_98\{1,}/nr_98{1,}/' "${OUTPUT}"

echo "Succesfully generated JSON schema:"

if [ "$(which realpath)" != "" ]; then
  realpath "${OUTPUT}"
else
  echo "${OUTPUT}"
fi
